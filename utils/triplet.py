import numpy as np
from tqdm import tqdm
from .readfile import*

def psi(hit1pos,hit2pos):
    dr = hit2pos[0]-hit1pos[0]
    dz = hit2pos[2]-hit1pos[2]
    return np.arctan2(dr,dz)



def psi_conected( d2_h1_pos,d2_h2_pos):

        dr2 = d2_h2_pos[0,:]-d2_h1_pos[0]
        dz2 = d2_h2_pos[2,:]-d2_h1_pos[2]
        return np.arctan2(dr2,dz2)

class triplet:

    "triplet contain three hits .. each vector is a list of tree values" 
    def __init__(self,hitids,pt,dtheta,label):
        self.hitids = hitids ##hit ids of each hit   dim - (1,3)     
        self.pt = pt  ##pt of a triplet
        self.dtheta = dtheta  ##dtheta of a triplet
        self.label  = label   ## wether triplet comes from target particle(signal) or not its a bool number 

# class triplet:

#     "triplet contain three hits .. each vector is a list of tree values" 
#     def __init__(self,hitids,position,pids,label):
#         self.hitids = hitids ##hit ids of each hit   dim - (1,3)     
#         self.positions = position  ##in r phi z coordinate..each row represents r,phi,z of a hit..dim - (3,3)
#         self.pids = pids  ##pid of each hit,dim same as hits ..dim- (1,3)
#         self.label  = label   ## wether triplet comes from target particle() or not its a bool number 






def make_triplets(doublet_array,signal_df,hits_df,config,barrel_data):
    signal_df = add_barrel_numbers(signal_df,barrel_data)
    hits_df = add_barrel_numbers(hits_df,barrel_data)
    dtheta = config['dtheta']
    doublet_array = np.array(doublet_array)
    hit_triplet_origin = np.array(doublet_array[0].triplet_origin,dtype = bool)  ##getting all the hits that originate triplet
    hits_data =  doublet_array[hit_triplet_origin] ##collection of all the doublets that correspond to triplet origin hits
    hitids  = np.array(doublet_array[0].hits)
    triplet_array = []
    triplet_layer = []
    signal_pids = signal_df.particle_id.unique()
        
    for doublet1 in hits_data:
        connected_hits = doublet1.connected_hits_ids
        hit_barrel_num = hits_df.loc[hits_df['hit_id'] == doublet1.hit1_id, 'barrel_layers'].to_list()[0]
        dtheta = config['dtheta'][int(hit_barrel_num)]
        dkappa = config['dkappa'][int(hit_barrel_num)]
        for hit2ids in connected_hits:
            index =  np.where(hitids==hit2ids)[0]
            if(len(index)):
                doublet2 = doublet_array[index][0]
                psi1 = psi(doublet1.hit1_pos,doublet2.hit1_pos)
                psi2 = psi_conected(doublet2.hit1_pos,doublet2.connected_hits_pos)
                diff = psi2-psi1
               
                nexthit = np.where((diff>=dtheta[0]) & (diff<=dtheta[1]))[0]

                if(len(nexthit)):
                    for i in nexthit:
                            dk,pt = dkappa_for_triplets(doublet1.hit1_id,
                                                        doublet2.hit1_id,
                                                        doublet2.connected_hits_ids[i],config,hits_df)
                            
                            if(dk >= dkappa[0] and dk <= dkappa[1]):
                            
                                hits = [doublet1.hit1_id,doublet2.hit1_id,doublet2.connected_hits_ids[i]]
                                pids = [doublet1.pid1,doublet2.pid1,doublet2.pid2[i]]
                                all_equal = all(element == pids[0] for element in pids)
                                if(all_equal):
                                    if(np.isin(pids[0],signal_pids)):
                                            label = True
                                    else: label = False  
                                else: label = False
                                ####Triplet fit will be added here 
                                _triplet = triplet(hits,pt,diff[i],label)  
                                triplet_array.append(_triplet)
                                triplet_layer.append(hit_barrel_num)
    return triplet_array,triplet_layer


def make_triplets_with_pdg(doublet_array,signal_df,hits_df,pdf,config,barrel_data):
    signal_df = add_barrel_numbers(signal_df,barrel_data)
    hits_df = add_barrel_numbers(hits_df,barrel_data)
    dtheta = config['dtheta']
    doublet_array = np.array(doublet_array)
    hit_triplet_origin = np.array(doublet_array[0].triplet_origin,dtype = bool)  ##getting all the hits that originate triplet
    hits_data =  doublet_array[hit_triplet_origin] ##collection of all the doublets that correspond to triplet origin hits
    hitids  = np.array(doublet_array[0].hits)
    triplet_array = []
    triplet_layer = []
    triplet_pdgid = []
    signal_pids = signal_df.particle_id.unique()
        
    for doublet1 in hits_data:
        connected_hits = doublet1.connected_hits_ids
        hit_barrel_num = hits_df.loc[hits_df['hit_id'] == doublet1.hit1_id, 'barrel_layers'].to_list()[0]
        for hit2ids in connected_hits:
            index =  np.where(hitids==hit2ids)[0]
            if(len(index)):
                doublet2 = doublet_array[index][0]
                psi1 = psi(doublet1.hit1_pos,doublet2.hit1_pos)
                psi2 = psi_conected(doublet2.hit1_pos,doublet2.connected_hits_pos)
                diff = np.abs(psi2-psi1)
         
                nexthit = np.where(np.abs(diff)<dtheta)[0]

                if(len(nexthit)):
                    for i in nexthit:
                            
                            hits = [doublet1.hit1_id,doublet2.hit1_id,doublet2.connected_hits_ids[i]]
                            position = np.vstack((doublet1.hit1_pos,
                                                doublet2.hit1_pos,
                                                doublet2.connected_hits_pos[:,i]                                           
                                                ))

                            pids = [doublet1.pid1,doublet2.pid1,doublet2.pid2[i]]
                            all_equal = all(element == pids[0] for element in pids)
                            if(all_equal):
                                if(np.isin(pids[0],signal_pids)):
                                        ptype = pdf.loc[pdf.particle_id==pids[0],'particle_type'].values[0]
                                        triplet_pdgid.append(ptype)
                                        label = True
                                else: label = False  
                            else: label = False

                            _triplet = triplet(hits,position,pids,label)
                            triplet_array.append(_triplet)
                            triplet_layer.append(hit_barrel_num)
    return triplet_array,triplet_layer,triplet_pdgid



def calculate_signal_triplets(signal_df):
    barrel_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    signal_triplets = 0
    for pid in signal_df.particle_id.unique():
        df = signal_df[signal_df.particle_id==pid]
        
        frequency_array = np.array([np.sum(df['barrel_layers'].values == element) for element in barrel_num])

        for i in range(8):
            triplet_layer = frequency_array[i:i+3]
            all_nonzero = np.all(triplet_layer != 0)
            if all_nonzero:
                signal_triplets += np.prod(triplet_layer) 
    return signal_triplets        


            
def calculate_triplet_metrics(triplet_array,signal_df):
    constructed_true_triplets = 0
    total_constructed_triplets = len(triplet_array)
    for triplets in triplet_array:
        constructed_true_triplets += triplets.label

    signal_triplets = calculate_signal_triplets(signal_df)
    eff = constructed_true_triplets/signal_triplets 
   
    pur = constructed_true_triplets/total_constructed_triplets     
    return eff,pur,total_constructed_triplets,constructed_true_triplets  





   
####for polar angle calculations




def get_hitsdf_for_pdgid(signal_df,pdf,pdg_id):

    pids = pdf[pdf.particle_type.isin(pdg_id)]['particle_id'].unique()
    hdf_for_pid = signal_df[signal_df.particle_id.isin(pids)]
    return hdf_for_pid.reset_index(drop = True),hdf_for_pid.particle_id.unique()


def calculate_polar_angle(pdg_id,config,barrel_data):
    print(pdg_id)    
    dtheta = np.array([])

    for eno in tqdm(range(90)):
        hdf,pdf  = read_files(eno)

        signal_df = get_signal(hdf,pdf,config)
        signal_df = add_barrel_numbers(signal_df,barrel_data)

        pdg_df,pdg_pids = get_hitsdf_for_pdgid(signal_df,pdf,pdg_id)

        
        for pid in pdg_pids:

            _df = pdg_df[pdg_df.particle_id==pid]

            barrel_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            frequency_array = np.array([np.sum(_df['barrel_layers'].values == element) for element in barrel_num])
            for i in range(8):
                triplet_layer = frequency_array[i:i+3]
                all_nonzero = np.all(triplet_layer != 0)
                if all_nonzero:
        

                    triplet_df = _df[_df.barrel_layers.isin(barrel_num[i:i+3])]
                    rl1 = triplet_df[triplet_df.barrel_layers==barrel_num[i]]['r'].values
                    rl2 = triplet_df[triplet_df.barrel_layers==barrel_num[i+1]]['r'].values
                    rl3 = triplet_df[triplet_df.barrel_layers==barrel_num[i+2]]['r'].values


                    zl1 = triplet_df[triplet_df.barrel_layers==barrel_num[i]]['z'].values
                    zl2 = triplet_df[triplet_df.barrel_layers==barrel_num[i+1]]['z'].values
                    zl3 = triplet_df[triplet_df.barrel_layers==barrel_num[i+2]]['z'].values


                    for j in range(len(rl1)):
                        for k in range(len(rl2)):
                            dr1  = rl2[k]-rl1[j]
                            dz1 = zl2[k]-zl1[j]
                            theta1 = np.arctan2(dr1,dz1)
                    
                            for l in range(len(rl3)):
                                dr2 = rl3[l]-rl2[k]
                                dz2 = zl3[l]-zl2[k]
                                dtheta = np.append(dtheta,np.arctan2(dr2,dz2)-theta1)
    print("finished")                            
    return dtheta



def dkappa_for_triplets(hit1id,hit2id,hit3id,config,hits_df):
    ''' the function return dkappa as well as pt for the triplet'''
     
    x1 = hits_df.loc[hits_df.hit_id==hit1id,'x'].values[0]
    y1 = hits_df.loc[hits_df.hit_id==hit1id,'y'].values[0]

    x2 = hits_df.loc[hits_df.hit_id==hit2id,'x'].values[0]
    y2 = hits_df.loc[hits_df.hit_id==hit2id,'y'].values[0]
    
    x3 = hits_df.loc[hits_df.hit_id==hit3id,'x'].values[0]
    y3 = hits_df.loc[hits_df.hit_id==hit3id,'y'].values[0]
    k013 = calculate_curvature(0,0,x1,y1,x3,y3)
    k123 = calculate_curvature(x1,y1,x2,y2,x3,y3)

    pt = 0.3*config['B']/k123
    

    return k013-k123,pt



    


def calculate_curvature(x1, y1, x2, y2, x3, y3):
    r12_x = x2 - x1
    r12_y = y2 - y1
    r13_x = x3 - x1
    r13_y = y3 - y1
    r23_x = x3 - x2
    r23_y = y3 - y2
    
    r13_mod = np.sqrt(r13_x**2 + r13_y**2)
    r12_mod = np.sqrt(r12_x**2 + r12_y**2)
    r23_mod = np.sqrt(r23_x**2 + r23_y**2)
    
    num = abs(r23_x * r12_y - r12_x * r23_y)
    
    return 2 * num / (r12_mod * r13_mod * r23_mod)

