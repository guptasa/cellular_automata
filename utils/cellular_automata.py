import numpy as np

# class cellular_automata():
#     def __init__(self,index_dict,grid):
#         self.index_dict = index_dict
#         self.grid = grid


#     def start_evolution(self):
#         for i in range(1,8):
#            for iter,neighbors in enumerate(self.index_dict[i]):
#                 if(len(neighbors)):
#                     connected_values = self.grid[i-1][np.array(neighbors)]
#                     self.grid[i][iter]  = connected_values.max()+1
          
import numpy as np



def build_graph(triplet_array,triplet_df):
    ''' 
    Build graph of triplets . It returns an index array which is a dict.
      for example region 1st elecment will contain infor about prev region triplets
    note- TRIPLET_DF and TRIPLET_LAYER are same
    '''

    triplet_array,triplet_layer = np.array(triplet_array),np.array(triplet_df)
    layers = [0,1,2,3,4,5,6,7]  
    pt_arr = []

    for trip in triplet_array:
        pt_arr.append(trip.pt)

    pt_arr  = np.array(pt_arr) 
    index_dict = {i:[] for i in range(1,8)}
    for layer in range(1,8):
      
        

        pt_cut = 20000  
        
        index_layer = np.where(triplet_layer == layer)[0]
        prev_layer_index = np.where(triplet_layer == layer-1)[0]
        
        prev_layer_pt_arr = pt_arr[prev_layer_index]
        
        prev_layer_triplets = triplet_array[prev_layer_index]
        current_layer_triplets = triplet_array[index_layer]
        for ml in range(len(current_layer_triplets)):
            triplet_ = current_layer_triplets[ml]
            neighbors = []
            l1_triplet_hits = triplet_.hitids
            pt = triplet_.pt
            # pt_bin_idx = np.where((prev_layer_pt_arr<pt+pt_cut)&(prev_layer_pt_arr>pt-pt_cut))[0]
            for idx in range(len(prev_layer_triplets)):
                    i0_triplet_hits  = prev_layer_triplets[idx].hitids  #hit ids in previous layer
          
                    if(i0_triplet_hits[1]==l1_triplet_hits[0] and i0_triplet_hits[2]==l1_triplet_hits[1]):
                        neighbors.append(idx)
            index_dict[layer].append(neighbors)  
    grid = {i:np.ones(len(np.where(triplet_layer==i)[0])) for i in range(8)}
    ca = cellular_automata(index_dict,grid)
    return ca

class cellular_automata():
    def __init__(self,index_dict,grid):
        self.index_dict = index_dict
        self.grid = grid
        self.track_collection =[]
        self.start_evolution()
        self.collect_tracks()



    def start_evolution(self):
        for i in range(1,8):
           for iter,neighbors in enumerate(self.index_dict[i]):
                if(len(neighbors)):
                    connected_values = self.grid[i-1][np.array(neighbors)]
                    self.grid[i][iter]  = connected_values.max()+1
          
    
    
    
    def collect_tracks(self):
        ##below command is temporary..will get changed if i start ca evolution with valuse zero for each cells

        for i in range(8):
            self.grid[i] = self.grid[i]-1  
        idxs = np.where(self.grid[7]==7)[0]
        for idx in idxs:
            traversing_track,node_split_dict = self.get_track_graph(idx)
            _ = self.track_builder(7,idx,[idx],traversing_track,node_split_dict)

    
    

    def valid_neighbors(self,idx,iter):
        '''
        return a valid neighbors with the aprropriate value of the grid
        '''
        neighbors = self.index_dict[iter][idx]

        valid_nbrs = []
        for n in neighbors:
            if(self.grid[iter-1][n]==iter-1):
            
                valid_nbrs.append(n)
        return valid_nbrs 
    
    def make_nodesplit_dict(self,traversing_track):

        '''
        this function is important for spotting the node where it splits into more than one node
        '''

        node_splits = {i:[] for i in range(8)}
        for i in range(7,0,-1):

            current_node = traversing_track[i]
            current_node = [item for sublist in current_node for item in sublist]


            for n in range(len(current_node)):
                neighbor_node = traversing_track[i-1][n]
                if(len(neighbor_node)>1):
                    node_splits[i].append(len(neighbor_node))
                else:
                    node_splits[i].append(0)    

        return node_splits
    
    def get_track_graph(self,idx):
    
        current_node = [idx]
        iter = 7
        total_tracks = []

        traversing_track = {i:[] for i in range(8)}
        traversing_track[7] = [current_node]
        while True:

            neighbors = []
            for idx in current_node:
                neighbors.append(self.valid_neighbors(idx,iter))
            iter = iter-1
            traversing_track[iter] = neighbors
            current_node = [item for sublist in neighbors for item in sublist]
            if(iter==0):
                break
        node_split_dict = self.make_nodesplit_dict(traversing_track)
        return traversing_track,node_split_dict
        
    
    def track_builder(self,region,node,track_chain,traversing_track,node_split_dict):
    
        '''
        give track chain until the node splits

        '''

        
        stack = []
        while True:
        
            if(len(track_chain)==8):
            
                self.track_collection.append(track_chain.copy())
            
                break
        
            current_region_nodes = traversing_track[region]
            current_nodes_flatten = [item for sublist in current_region_nodes for item in sublist]
            
            index = current_nodes_flatten.index(node)
            neighbor_nodes = traversing_track[region-1][index]
            
            
            
            if(len(neighbor_nodes)==1):
                track_chain.append(neighbor_nodes[0]) 
                region = region-1
                node = neighbor_nodes[0]
            else:
                stack = (region,index,len(neighbor_nodes))
                

                for nodes_2 in neighbor_nodes:
                    # print(track_chain)
                    track_chain_copy = track_chain.copy()
                    track_chain_copy.append(nodes_2)

                    new_track_chain = self.track_builder(region-1,nodes_2,track_chain_copy,traversing_track,node_split_dict)
                    # print(f"new_track_chain = {new_track_chain}")

                    track_chain_copy.extend(new_track_chain)
                    # print(track_chain_copy)
                break    
                
        return track_chain



