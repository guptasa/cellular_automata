import numpy as np
# // Global general fit of fitted triplets on a track
# // uses sigmaMS from previous global fit if rerun=true

# // Make initial vectors/matrices
# // (everything is zero initialized)


##i could have created dictionary for this, but later there might be some addiytional information related to track that need to be 
##included so class is the better way for the same. 

class Global_Fit:
    def __init__(self):
        self.C3D_glob = 0
        self.sigma_C3D_glob = 0
        self.chi2_glob = 0

def GTTF(triplet_list):
    '''
    the input to this function is list of triplets objects. For each triplet, the local fit is performed i.e. 
    by including multiple scattering and hit uncertainties. The output of the local fit is used to perform the global fit.
    '''

    NtripletsOnTrack  = len(triplet_list)
    NTotalHits = NtripletsOnTrack+2


    #  // Count number of hit uncertainty directions of all hits to allocate vectors/matrices
    N_DirTotal =NTotalHits*2 # this is bit different than abhi's code because we have two directions for each hit u and v for the barrel



    # Initialize the vectors
    rho = np.zeros(2 * NtripletsOnTrack)
    psi = np.zeros(2 * NtripletsOnTrack)

    # Initialize the matrices
    D_MS = np.zeros((2 * NtripletsOnTrack, 2 * NtripletsOnTrack))
    D_hit = np.zeros((N_DirTotal, N_DirTotal))

    H_theta = np.zeros((NtripletsOnTrack, N_DirTotal))
    H_phi = np.zeros((NtripletsOnTrack, N_DirTotal))


        
    for i in range(NtripletsOnTrack):
        t_i = triplet_list[i]  ##acceing triplet

        ##rho and psi matrix
        rho[i] = t_i.rho_theta
        rho[i+NtripletsOnTrack] = t_i.rho_phi

        psi[i] = t_i.theta0
        psi[i+NtripletsOnTrack] = t_i.phi0

        sigma2_MS = t_i.sigmaMS_gen**2

        D_MS[i,i] = 1/sigma2_MS
        D_MS[i+NtripletsOnTrack,i+NtripletsOnTrack] = ((np.sin(0.5 * (t_i.theta_1C + t_i.theta_2C))) ** 2) / sigma2_MS

        posDeriv_idx = 0
        for j in range(3):
            hit_idx = i + j  # unique idx of hit in a track
            
            # hit uncertainty directions
            for dir in range(2):
                # set diagonal elements in the block corresponding to this hit uncertainty direction
                block_start = dir * NTotalHits
                D_hit[block_start + hit_idx, block_start + hit_idx] = 1. / np.linalg.norm(t_i.hit_dictionary[j]['shift'][dir]) ** 2

                H_theta[i, block_start + i + j] = t_i.h_theta[posDeriv_idx]
                H_phi[i, block_start + i + j] = t_i.h_phi[posDeriv_idx]

                posDeriv_idx += 1


    # Concatenate H_theta and H_phi vertically
    H = np.vstack((H_theta, H_phi))

    # Compute the inverse of the K matrix
    # Assuming D_MS and D_hit are diagonal matrices, you can directly invert them
    D_MS_inv = np.linalg.inv(D_MS)
    D_hit_inv = np.linalg.inv(D_hit)

    # Compute K_inv
    K_inv = D_MS_inv + H @ D_hit_inv @ H.T

    # Compute K by inverting K_inv
    K = np.linalg.inv(K_inv)

    C3D_glob_num = (-1.0 * rho.T @ K @ psi).item()
    C3D_glob_deno = (rho.T @ K @ rho).item()
    psiT_K_psi = (psi.T @ K @ psi).item()
    C3D_glob = C3D_glob_num / C3D_glob_deno
    sigmaC3D_glob = 1.0 / np.sqrt(C3D_glob_deno)
    chi2_glob = psiT_K_psi - np.power(C3D_glob / sigmaC3D_glob, 2)


    gttf_class = Global_Fit()
    gttf_class.C3D_glob = C3D_glob
    gttf_class.sigma_C3D_glob = sigmaC3D_glob
    gttf_class.chi2_glob = chi2_glob

    return gttf_class