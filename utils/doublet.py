import numpy as np
from .readfile import get_layer_df

import pandas as pd

class doublet:
    hits_ = []  ##will help to find the index of classes 
    triplet_origin_ = []  ##calculate wether a triplets originate from a hit or not
                            ##for example for N layers hit in layers N-1 and N wont originate any triplet 

    def __init__(self, hit1_pos, hit1_id, connected_hits_pos, connected_hits_ids, labels,pid1,pid2):
        self.hit1_pos = hit1_pos
        self.hit1_id = hit1_id
        self.connected_hits_pos = connected_hits_pos
        self.connected_hits_ids = connected_hits_ids
        self.labels = labels   ##note that in during the construction stage it labels a doublet true if pid1==pid2 ..but essentialy it does not need to be signal..however when eff is calculated in the function -eff and pur. i do take intoo account to that wether it belongs to signal pids or not 
        self.pid1 = pid1
        self.pid2 = pid2


    def istrue(self,barrel_df):   ##returns wether a doublet is true or not 
        pid1 = barrel_df[barrel_df["hit_id"]==self.hit1_id]["particle_id"].iloc[0]
        pid2 = barrel_df[barrel_df["hit_id"]==self.hit2_id]["particle_id"].iloc[0]  


        if(pid1==pid2 and pid1!=0):
            return True
        else: False 
    def set_hits(self):
        self.hits = self.hits_
        self.triplet_origin = self.triplet_origin_
    @classmethod
    def initialize_class_variables(cls):
        cls.hits_ = []
        cls.triplet_origin_ = []

# Create the DataFrame

  


def make_all_doublets(df,barrel_data,config):
    doublet_arr = []
    doublet.initialize_class_variables()
 
    for i in range(len(barrel_data['dphi'])):
        _in = i
        _out = i+1

        lin_df = get_layer_df(df,barrel_data['barrel_layers'][_in])
        lout_df = get_layer_df(df,barrel_data['barrel_layers'][_out])


        config['r_in'] = barrel_data['radius'][_in]
        config['r_out'] = barrel_data['radius'][_out]
        config['dphi'] = barrel_data['dphi'][_in]
        doublet_arr.extend(make_doublets(lin_df,lout_df,config, _in)) 
 
    doublet_arr[0].set_hits()
    return doublet_arr



def make_doublets(lin_df,lout_df,config,layer):  ##create all possible doublets between two layers and append the global array - doublets 
    z0 = config['z0']
    z0_bound = z0[layer]

    config['lower_bound'] = z0_bound[0]
    config['upper_bound'] = z0_bound[1]

    doublet_in_layers = []
    if(len(lin_df)!=0 and len(lout_df)):
        for index ,hit_a in lin_df.iterrows():

            df_z = apply_z_cut(hit_a,lout_df,config)
          
            df_phi = apply_phi_cut(df_z,hit_a.phi,config['dphi'])

            if(len(df_phi)!=0):
                    
                connected_hits_pos = np.array([df_phi.r,df_phi.phi,df_phi.z])
                _doublet = doublet(np.array([hit_a.r,hit_a.phi,hit_a.z]),
                                hit_a.hit_id,
                                connected_hits_pos,
                                df_phi.hit_id.values,
                                (df_phi.particle_id==hit_a.particle_id).values,
                                hit_a.particle_id,
                                df_phi.particle_id.values
                                )

                doublet_in_layers.append(_doublet)

                doublet.hits_.append(hit_a.hit_id)

                if(hit_a.volume_id==17):

                    doublet.triplet_origin_.append(0)
                else : 
                    doublet.triplet_origin_.append(1)
    return doublet_in_layers        
                




def total_signal_doublets(signal_df,barrel_data):
    true_doublets = 0 
    
    for pid in signal_df.particle_id.unique():
        _df = signal_df[signal_df.particle_id==pid]
        frequency = np.array([_df[(_df['volume_id'] == tpl[0]) & (_df['layer_id'] == tpl[1])].shape[0] for tpl in barrel_data['barrel_layers']])
        
        true_doublets += np.sum(frequency[:-1] * frequency[1:])
    return true_doublets



def eff_and_pur(reco_doublet_arr,signal_df,barrel_data):
    signal_truth = 0
    total_doublets = 0
    
    pid = signal_df.particle_id.unique()
    for doublets in reco_doublet_arr:
        true_doublets = sum(doublets.labels)
      
        if(true_doublets):

            if(np.isin(doublets.pid1,pid)):
               signal_truth += true_doublets

        total_doublets += len(doublets.labels)

    pur = signal_truth/total_doublets 
    signal_doublets = total_signal_doublets(signal_df,barrel_data)

    eff = signal_truth/signal_doublets

    return eff,pur,total_doublets,signal_doublets




def normalize_angle(angle):  ##for phi
    """
    Normalize an angle to be within the range [-pi, pi].
    
    Args:
    angle (float): The angle to normalize, in radians.
    
    Returns:
    float: The normalized angle.
    """
    # Calculate the modulo of the angle with respect to 2π
    normalized_angle = angle %(2*np.pi)
    # Shift the angle to be in the range [-π, π]
    if normalized_angle > np.pi:
        normalized_angle -= 2 * np.pi
    return normalized_angle    
    
def apply_z_cut(hit_df, louter_df,config):
    '''
    Gets hit position and returns the xyz coordinate in the next barrel layer of the zslice
    radius of next layer
    
    '''

    zup = (config['r_out'] / hit_df.r) * (hit_df.z +  abs(config['lower_bound'])) - abs(config['lower_bound'])
    zdown = (config['r_out'] / hit_df.r) * (hit_df.z - config['upper_bound']) + config['upper_bound']
    z_cut =   np.sort(np.array([zup, zdown]))


    return louter_df[(louter_df.z>=z_cut[0]) & (louter_df.z<=z_cut[1])]


def apply_phi_cut(df,phi,dphi):
 ##apply phi cut to given data frame
    phi1  = normalize_angle(phi-dphi)
    phi2  =  normalize_angle(phi+dphi)

    if(abs(phi1)>2 and abs(phi2)>2):

        if(phi1>0 and phi2<0):  ##around pi boundary 

            df_phi_cut1= df[(df.phi>=phi1) & (df.phi<=np.pi)]
            df_phi_cut2 = df[(df.phi>=-np.pi) & (df.phi<=phi2)]

            return pd.concat([df_phi_cut1,df_phi_cut2])

        elif(phi1<0 and phi2>0):
              
            df_phi_cut1= df[(df.phi>=phi2) & (df.phi<=np.pi)]
            df_phi_cut2 = df[(df.phi>=-np.pi) & (df.phi<=phi1)]

            return pd.concat([df_phi_cut1,df_phi_cut2])
        else:
            return df[(df.phi>=phi1) & (df.phi<=phi2)]   

    else:
        return  df[(df.phi>=phi1) & (df.phi<=phi2)]   
