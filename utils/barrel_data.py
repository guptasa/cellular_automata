## this file calculates dphi from the radius of barrel layers
from .readfile import*
###the center of a projected circle (to find a dphi window)
def center(x,y,r ):
    rho = x**2+y**2

    a  = 4*rho
    b = -4*x*rho
    c = rho**2-4*(r**2)*(y**2)
    x1,x2  =  np.roots([a,b,c]) 

    y1 = (rho-2*x*x1)/(2*y)
   
   
    return x1,y1

##this function calculates intersection point of upper circle with barrel 
def pts_on_circle(alpha,beta,r2):
    rho = alpha**2+beta**2

    a  = 4*rho
    b = -4*alpha*r2**2
    c = r2**4-4*(beta**2)*(r2**2)
    
    x1,x2  =  np.roots([a,b,c]) 
    
    y1 = (r2**2-2*alpha*x1)/(2*beta)
    y2 =(r2**2-2*alpha*x2)/(2*beta)
   
    if(x1>0 and y1>0):
        return x1,y1 
    elif(x2>0 and y2>0):
        return x2,y2




def dphi(r_in,r_out,rlim):
    theta = np.pi/4
    x = r_in*np.cos(theta)
    y = r_in*np.sin(theta) 


    alpha,beta = center(x,y,rlim)

    x2,y2 = pts_on_circle(alpha,beta,r_out)

    return abs(np.arctan2(y2,x2)-theta)



def get_barrel_data(config):

    pt = config['pt']  ##in GeV
    B = config['B']  ##in tesla
    r0 = pt*(1/(0.3*B))*1000 ##in mm

    
    hdf,pdf = read_files(0)
    barrel_df = load_barrel_df(hdf) ##taking any random event 
    
    
    
    barrel_df['r'] = np.sqrt(barrel_df.x**2+barrel_df.y**2)
    barrel_df['phi'] = np.arctan2(barrel_df.y,barrel_df.x)
    barrel_layers = [(8,2),(8,4),(8,6),(8,8),(13,2),(13,4),(13,6),(13,8),(17,2),(17,4)]  ## according to trackml dataset    
    barrel_data  = {'barrel_layers':barrel_layers,'radius':[],'dphi':[]}
    for i in range(len(barrel_data['barrel_layers'])):

        ldf = get_layer_df(barrel_df,barrel_data['barrel_layers'][i])
        barrel_data['radius'].append(ldf.r.mean())

        if(i>0):
            r_in = barrel_data['radius'][i-1]
            r_out = barrel_data['radius'][i]
            barrel_data['dphi'].append(dphi(r_in,r_out,r0))


    return barrel_data;     