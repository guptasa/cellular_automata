import numpy as np
import pandas as pd

cotangent = lambda x: 1 / np.tan(x)
detector_df = pd.read_csv('control_data/detectors.csv')

############################################################################################
############################################################################################
############################################################################################

def main_triplet_fit(triplet_element,hits_df,config):
    linearizetriplet(triplet_element,config,hits_df)
    calculate_hit_pos_dev(triplet_element,hits_df)
    doGeneralTripletFit(triplet_element)



def linearizetriplet(triplet_element,config,hits_df):
    '''
    Given a triplet class element performs linearization and stores all the fundamental triplet parameters
    there are four triplet parameters
    '''

    get_triplets_coordiantes(triplet_element,config,hits_df)
    triplet_element.B  = config['B']
    x01 = triplet_element.space_points[1,:]-triplet_element.space_points[0,:]
    x12 = triplet_element.space_points[2,:]-triplet_element.space_points[1,:]
    x02  = triplet_element.space_points[2,:]-triplet_element.space_points[0,:]


    ##trasner distance

    d01 = np.sqrt(x01[0]**2 + x01[1]**2)
    d12 = np.sqrt(x12[0]**2 + x12[1]**2)
    d02 = np.sqrt(x02[0]**2 + x02[1]**2)



    z01 = x01[2]
    z12 = x12[2]

    ##z component of the cross product
    rz  = x01[0]*x12[1]-x01[1]*x12[0]

    c_perp = 2*rz/(d01*d12*d02)


    # Find center of circle using x0 & x2 in transverse plane

    # mid-point

    mid = [] ##2 dim vector for mid position in x y plane 

    mid.append(0.5*(triplet_element.space_points[0,0]+triplet_element.space_points[2,0]))
    mid.append(0.5*(triplet_element.space_points[0,1]+triplet_element.space_points[2,1]))

    normal = [] ##2 dim vector for normal vector in x y plane
    normal.append((triplet_element.space_points[2,1]-triplet_element.space_points[0,1])/d02)
    normal.append(-(triplet_element.space_points[2,0]-triplet_element.space_points[0,0])/d02)   

    ##distance to center along prependicular

    prep_d = np.sqrt(1/(c_perp**2) - (d02*d02)/4.)

    center = [] ##2 dim vector for center in x y plane [[c11,c12][c21,c22]]

    first_element = [] ##2d vector for elements in center

    first_element.append(mid[0]+prep_d*normal[0])
    first_element.append(mid[1]+prep_d*normal[1])

    center.append(first_element)

    second_element = [] ##2d vector for elements in center

    second_element.append(mid[0]-prep_d*normal[0])
    second_element.append(mid[1]-prep_d*normal[1])

    center.append(second_element)

    ##second hit x,y cordinate
    x1 = np.array([triplet_element.space_points[1,0],triplet_element.space_points[1,1]])
    # assert np.sum((np.array(x1) - np.array(mid))**2) != 0

    
# Find the correct center
    c_correct = np.array([0.0, 0.0])  # Default
    for c_i in center:
        if np.dot(x1 - np.array(mid), c_i - np.array(mid)) < 0:
            c_correct = c_i
            break

    # assert np.sum(c_correct**2) != 0  # Equivalent to c_correct.Mag2() != 0 in ROOT


# Find the tangent to the track in 2D at the scattering plane

    r1 = np.array(x1) - np.array(c_correct)

    tangent2D = np.array([r1[1], -1.0 * r1[0]])
    if np.dot(tangent2D, x12[0:2]) < 0:
        tangent2D *= -1

    phi_1C = 2.0 * np.arcsin(0.5 * d01 * c_perp)
    phi_2C = 2.0 * np.arcsin(0.5 * d12 * c_perp)

    triplet_element.phi_1C = phi_1C
    triplet_element.phi_2C = phi_2C

    phi2_1C = phi_1C**2
    phi2_2C = phi_2C**2
    sin2_0p5phi1C = np.sin(0.5 * phi_1C)**2
    sin2_0p5phi2C = np.sin(0.5 * phi_2C)**2

    # Calculate 3D curvatures
    c_3D_1C = phi_1C / np.sqrt(z01**2 + (d01**2) * phi2_1C / (4.0 * sin2_0p5phi1C))
    c_3D_2C = phi_2C / np.sqrt(z12**2 + (d12**2) * phi2_2C / (4.0 * sin2_0p5phi2C))
    triplet_element.c_3D_1C = c_3D_1C
    triplet_element.c_3D_2C = c_3D_2C

    # Calculate polar angles
    theta_1C = np.arccos(z01 * c_3D_1C / phi_1C)
    theta_2C = np.arccos(z12 * c_3D_2C / phi_2C)

    triplet_element.theta_1C = theta_1C
    triplet_element.theta_2C = theta_2C

    # Calculate the sin of the average polar angle
    sin_theta = np.sin(0.5 * (theta_1C + theta_2C))
    tangent2D_norm = sin_theta / np.sqrt(np.sum(tangent2D**2)) * tangent2D

    # Normalize tangent in 3D
    tangent3D = np.array([tangent2D_norm[0], tangent2D_norm[1], np.cos(0.5 * (theta_1C + theta_2C))])

    # Estimate sigma_MS from the 3D curvatures
    phi   = hits_df[hits_df.hit_id==triplet_element.hitids[1]]['phi'].values[0]
    sensor_normal = [np.cos(phi), np.sin(phi), 0.0]

    vid = hits_df[hits_df.hit_id==triplet_element.hitids[1]]['volume_id'].values[0]
    lid = hits_df[hits_df.hit_id==triplet_element.hitids[1]]['layer_id'].values[0]

    det_material = detector_df[(detector_df.volume_id==vid) & (detector_df.layer_id ==lid)]['module_t'].values[0]
   
    t_eff =  (det_material)  / np.dot(tangent3D, sensor_normal)

    triplet_element.t_eff = t_eff


    sigma_MS = np.abs(0.5 * (triplet_element.c_3D_1C + triplet_element.c_3D_2C)) * 45.0 * np.sqrt(t_eff) / config['B'] * (1 + 0.038 * np.log(t_eff))

    # Index parameters
    n_1C = (d01**2 + 4.0 * z01**2 * sin2_0p5phi1C / phi2_1C) / (d01**2 * phi_1C * np.sin(phi_1C) / (4.0 * sin2_0p5phi1C) + 4.0 * z01**2 * sin2_0p5phi1C / phi2_1C)
    n_2C = (d12**2 + 4.0 * z12**2 * sin2_0p5phi2C / phi2_2C) / (d12**2 * phi_2C * np.sin(phi_2C) / (4.0 * sin2_0p5phi2C) + 4.0 * z12**2 * sin2_0p5phi2C / phi2_2C)

    # Define cotangent function
    cot = lambda theta: np.cos(theta) / np.sin(theta)

    # Triplet parameters
    phi0 = 0.5 * (phi_1C * n_1C + phi_2C * n_2C)
    theta0 = theta_2C - theta_1C + ((1.0 - n_2C) * cot(theta_2C) - (1.0 - n_1C) * cot(theta_1C))
    rho_phi = -0.5 * (phi_1C * n_1C / c_3D_1C + phi_2C * n_2C / c_3D_2C)
    rho_theta = (1.0 - n_1C) * cot(theta_1C) / c_3D_1C - (1.0 - n_2C) * cot(theta_2C) / c_3D_2C    

    triplet_element.phi0 = phi0
    triplet_element.theta0 = theta0
    triplet_element.rho_phi = rho_phi
    triplet_element.rho_theta = rho_theta
    triplet_element.sigmaMS_lin = sigma_MS
    triplet_element.tangent3D = tangent3D
############################################################################################
############################################################################################
############################################################################################



def calculate_hit_pos_dev(triplet_element,hits_df):

    #     // Calculate directional derivatives of the kink angles
    # // w.r.t hit position shifts of every hit in triplet
    # // N_dir derivatives for every hit

    multiplier = 1  #  hits shifted by multiplier * sigma in every hit uncertainty direction

    ##array to store directional derivative

    h_theta = []
    h_phi = []

    ##loop over hits and hit position shift directions
    hit_dictionary  = {0:{},1:{},2:{}} ##dictionary of dictionaries that store, rotation, translation and shift matrix for each hit
    for hit in range(3):
        hitid = triplet_element.hitids[hit]
        
        rotation,tranlation,shift = get_rot_and_trans_and_shift_matrix(hitid,hits_df)
        hit_dictionary[hit]['rotation'] = rotation
        hit_dictionary[hit]['tranlation'] = tranlation
        hit_dictionary[hit]['shift'] = shift
        ##introducing shift in local u and v direction . that is why loop runs over twice
  
        for direction in range(2):

            new_space_points = triplet_element.space_points.copy()

            pos_vector = new_space_points[hit].reshape(3,1)
            local_vector = np.linalg.inv(rotation)@(pos_vector - tranlation)
            shift_vector = shift[:,direction].reshape(3,1)

            new_local_vector = local_vector + multiplier*shift_vector
            new_pos_vector = rotation@new_local_vector + tranlation

            new_space_points[hit] = new_pos_vector.reshape(3)

            phi,theta = quick_linearize(new_space_points)
    
            delta = np.sqrt(np.sum((multiplier*shift_vector)**2))

            h_theta.append((theta - triplet_element.theta0)/delta)
            h_phi.append((phi - triplet_element.phi0)/delta)
    triplet_element.hit_dictionary = hit_dictionary        
    triplet_element.h_theta = h_theta
    triplet_element.h_phi = h_phi
############################################################################################
############################################################################################
############################################################################################



def doGeneralTripletFit(triplet_element):
    #   // General triplet fit
    # // assumes linearization(around circ. & w.r.t hit posn. shifts)

    # // calculate projected angular uncertainties - Gammas
    Gamma_theta_theta = 0
    Gamma_phi_phi = 0
    Gamma_theta_phi = 0

    posDeriv_idx = 0
    for hit in range(3):
        for direction in range(2):

            hit_uncertainty_vector = triplet_element.hit_dictionary[hit]['shift'][:,direction]
            sigma_square =   np.sum(hit_uncertainty_vector**2)

            Gamma_theta_theta += (triplet_element.h_theta[posDeriv_idx]**2 )*sigma_square
            Gamma_phi_phi += (triplet_element.h_phi[posDeriv_idx]**2 )*sigma_square
            Gamma_theta_phi += (triplet_element.h_theta[posDeriv_idx]*triplet_element.h_phi[posDeriv_idx] )*sigma_square
            posDeriv_idx+=1


    ##estimate polar angle 
    theta = 0.5*(triplet_element.theta_1C + triplet_element.theta_2C)

    sigma_MS = triplet_element.sigmaMS_lin

    # // Choose correct uncertainties to take into account in the general fit

    Gamma_ttstar = Gamma_theta_theta + sigma_MS**2

    Gamma_ppstar = Gamma_phi_phi + (sigma_MS/np.sin(theta))**2


    denom = (triplet_element.rho_theta**2)*Gamma_ppstar+(triplet_element.rho_phi**2)*Gamma_ttstar-2*triplet_element.rho_theta*triplet_element.rho_phi*Gamma_theta_phi

    c_3D_min = -1.*(triplet_element.theta0*triplet_element.rho_theta*Gamma_ppstar+triplet_element.phi0*triplet_element.rho_phi*Gamma_ttstar - Gamma_theta_phi*(triplet_element.phi0*triplet_element.rho_theta+triplet_element.theta0*triplet_element.rho_phi))/denom

    sigma_c_3D  = np.sqrt((Gamma_ttstar*Gamma_ppstar-Gamma_theta_phi**2)/denom)

    chi2_min = (triplet_element.theta0*triplet_element.rho_phi-triplet_element.phi0*triplet_element.rho_theta)**2/denom

    triplet_element.sigmaMS_gen = abs(c_3D_min)*45.*np.sqrt(triplet_element.t_eff)/triplet_element.B*(1+0.038*np.log(triplet_element.t_eff))

    
    # // Kink angles 
    
    # // use fitted c3D to get total kink angles from the linearized relations

    # kink_theta = triplet_element.theta0 + c_3D_min*triplet_element.rho_theta
    # kink_phi = triplet_element.phi0 + c_3D_min*triplet_element.rho_phi

    # denom_kink = Gamma_ppstar*Gamma_ttstar-Gamma_theta_phi

    # kinkMS_theta = (triplet_element.sigmaMS_MS ** 2) * (kink_theta * Gamma_ppstar - kink_phi * Gamma_theta_phi) / denom_kink
    # kinkMS_phi =   ((triplet_element.sigmaMS_MS / np.sin(theta)) ** 2 )* (kink_phi * Gamma_ttstar - kink_theta * Gamma_theta_phi) / denom_kink

    triplet_element.C3D_gen = c_3D_min
    triplet_element.sigmaC3D_gen = sigma_c_3D
    triplet_element.chi2min_gen = chi2_min
    # triplet_element.thetaKinkMS_gen = kinkMS_theta
    # triplet_element.phiKinkMS_gen = kinkMS_phi

############################################################################################
############################################################################################
############################################################################################

def get_triplets_coordiantes(triplet_element,config,hits_df):

    trip_df = hits_df[hits_df['hit_id'].isin(triplet_element.hitids)]
    x = trip_df['x'].values
    y = trip_df['y'].values
    z = trip_df['z'].values
    r = trip_df['r'].values
  

    ###the code returns the coordinates of the hits in the triplet...first row corresponds to first hit and so on 
    coo8s  =  np.vstack((x,y,z))
    triplet_element.space_points = coo8s.T

    ##adding additional code for correctioin in material 
 
    theta = np.arctan((z[1]-z[0])/(r[1]-r[0]))

    triplet_element.magnetic_field = config['B']
    triplet_element.b = (45/config['B'])*np.sqrt(0.015/(np.cos(theta)))  ##this will later be used for sigma ms calculation

    
############################################################################################
############################################################################################
############################################################################################

    
def quick_linearize(space_points):
    """
    Take a 3x3 matrix of shifted hit coordinates and quickly calculate the linearized parameters.
    
    Args:
    - space_points: A 3x3 numpy array where each row represents the x, y, z coordinates of a hit.
                    Row 0: First hit [x, y, z]
                    Row 1: Second hit [x, y, z]
                    Row 2: Third hit [x, y, z]
                    
    Returns:
    - phi: Angle phi calculated from transverse plane.
    - theta: Angle theta calculated from longitudinal plane.
    """
    x_01 = space_points[1] - space_points[0]
    x_12 = space_points[2] - space_points[1]
    
    # Transverse (X-Y) Plane
    x_01_T = x_01[:2]  # X-Y components of x_01
    x_12_T = x_12[:2]  # X-Y components of x_12
    
    phi = np.arcsin(np.cross(x_01, x_12)[2] / np.sqrt(np.dot(x_01_T, x_01_T) * np.dot(x_12_T, x_12_T)))
    
    # Longitudinal (Z-S) Plane
    s_01 = np.sqrt(np.dot(x_01_T, x_01_T))
    s_12 = np.sqrt(np.dot(x_12_T, x_12_T))
    
    x_0_L = np.array([space_points[0, 2], 0., 0.])
    x_1_L = np.array([space_points[1, 2], s_01, 0.])
    x_2_L = np.array([space_points[2, 2], s_01 + s_12, 0.])
    
    x_01_L = x_1_L - x_0_L
    x_12_L = x_2_L - x_1_L
    
    theta = np.arcsin(np.cross(x_01_L, x_12_L)[2] / np.sqrt(np.dot(x_01_L, x_01_L) * np.dot(x_12_L, x_12_L)))
    
    return phi, theta
############################################################################################
############################################################################################
############################################################################################


def get_rot_and_trans_and_shift_matrix(hit,hits_df):
    '''
    hdf - hits df
    detector_df - detector csv file from trackml website
    hit = single hit id
    this function will also return two independent shift direction in local coordinate system

    '''
    ##getting hit specific dataframe

    _df = hits_df[hits_df.hit_id == hit]  ##df corresponding to particular hit
    ##getting volume,layer,module_id
    volume_id = _df.volume_id.values[0]
    layer_id = _df.layer_id.values[0]
    module_id = _df.module_id.values[0]

    
    # Filter the DataFrame to get the specific module's information
    module_info = detector_df[
        (detector_df['volume_id'] == volume_id) &
        (detector_df['layer_id'] == layer_id) &
        (detector_df['module_id'] == module_id)
    ]

    
    rot_x = module_info[['rot_xu', 'rot_xv', 'rot_xw']].values[0]
    rot_y = module_info[['rot_yu', 'rot_yv', 'rot_yw']].values[0]
    rot_z = module_info[['rot_zu', 'rot_zv', 'rot_zw']].values[0]
    
    # Construct the rotation matrix
    rotation_matrix = np.array([rot_x, rot_y, rot_z])
    
    # Extract the translation vector components
    translation_vector = module_info[['cx', 'cy', 'cz']].values[0].reshape(3, 1)
    
    du = module_info['pitch_u'].values[0]
    shift_vector1 = np.array([du/np.sqrt(12), 0, 0]).reshape(3, 1) 

    dv = module_info['pitch_v'].values[0]
    shift_vector2 = np.array([0, dv/np.sqrt(12), 0]).reshape(3, 1)

    shift_vector = np.hstack((shift_vector1, shift_vector2))




    return rotation_matrix, translation_vector,shift_vector



    
############################################################################################
############################################################################################
############################################################################################
