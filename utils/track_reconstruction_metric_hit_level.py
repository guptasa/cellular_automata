from .cellular_automata import *
import numpy as np
import pandas as pd
from .global_fit import GTTF


def is_track_signal(track,signal_pids,hits_df):
    '''
    Determines weather a track in the form of collection of hits is signal or not ..only if 100 percent of hits matches with signal pids 
    
    '''
    pid_ = hits_df[hits_df.hit_id.isin(track)]['particle_id'].values  ##finsding particle id for all hits in a track
    unique, counts = np.unique(pid_, return_counts=True)
    if(np.max(counts)==10):
        pid_selected = unique[np.argmax(counts)]
        if pid_selected in signal_pids:
            return True
        else:

            return False
    else:

        return False



def track_metric_hit_level(track_collection_hits,hits_df,signal_df):
    '''
    This function calculates the efficiency and purity of the track reconstruction
    not only that it also gives you the track collection in the form of hits
    which might be required for the later purposes
   
    '''    
    reconstructed_pids_50 = []
    reconstructed_pids_100  =  []


    for hits in track_collection_hits:  ##looping pover final tracks
       
        pid_ = hits_df[hits_df.hit_id.isin(hits)]['particle_id'].values  ##finsding particle id for all hits in a track
        unique, counts = np.unique(pid_, return_counts=True)
        if(np.max(counts)>=5):  ##50 % matching 

            most_common_elements = unique[np.argmax(counts)]    ##finding the most common particle id in a track
            if most_common_elements not in reconstructed_pids_50: ##if the particle id is not already reconstructed
                reconstructed_pids_50.append(most_common_elements)  ##append it to the reconstructed pids
            if(np.max(counts)==10):  ##criterion for 100% matching 
               
                if most_common_elements not in reconstructed_pids_100:
                    reconstructed_pids_100.append(most_common_elements)  ##append it to the reconstructed pids    
                
                           

    ##finding the true pids in the signal_df

    signal_pids = signal_df.particle_id.unique()
    
    
    reconstructed_signal_50 = 0
    for pid in reconstructed_pids_50:
        if pid in signal_pids:
            reconstructed_signal_50+=1

    ##for perfect matching efficiency 
    reconstructed_signal_100 = 0
    for pid in reconstructed_pids_100:
        if pid in signal_pids:
            reconstructed_signal_100+=1


    perfect_eff = reconstructed_signal_100/len(signal_pids)
    eff = reconstructed_signal_50/len(signal_pids)
    pur = reconstructed_signal_50/len(track_collection_hits)
    return eff,pur,perfect_eff



def disconnecting_the_graph_hit_level(track_collection,chi2_global,end):


    '''
    end specifies the region where you wanna trim the graph
    -1 means it trims from first hit from barrel layer 1
    zero means it trims from last barrel layer 10th 
    '''
    track_collection = np.array(track_collection)

    node_elements = track_collection[:,end] ###starting from region seven or region one
    unique_elements = np.unique(node_elements)

    track_after_trimming = []
    chi2_after_trimming = []

    for first_node in unique_elements:
        track_tree = track_collection[np.where(track_collection[:,end]== first_node)[0],:]

        ##only one of them will get selected 
        chi2_gttf_array= 1*chi2_global[np.where(track_collection[:,end]== first_node)[0]] ## array for storing track quality of each track

        filtered_track = track_tree[np.argmin(chi2_gttf_array)]

        # assert False
        track_after_trimming.append(filtered_track.tolist())
        chi2_after_trimming.append(np.min(chi2_gttf_array))

    return track_after_trimming,np.array(chi2_after_trimming)


def track_sharing_hits(track_collection,chi2):
    '''
    if two or many tracks share one hit, then track with least chi2_glob is selected
    '''
    track_collection = np.array(track_collection)
    filtered_tracks = []
    filtered_chi2 = []
    index_tracks = []

    for iter in range(len(track_collection)):

        if(iter not in index_tracks):
            index_tracks.append(iter)
                
            track = track_collection[iter]
            track_shairing_hits = [track]
            indices_for_current_track = [iter]
            ##find other tracks that share 50 % of the hits
            for iter2 in range(iter+1,len(track_collection)):
                if(iter2 not in index_tracks):

                    track2 = track_collection[iter2]
                    
                    common_triplets = np.sum(np.equal(track,track2))
              
                    if(common_triplets>=1):
                        index_tracks.append(iter2)
                        indices_for_current_track.append(iter2)
                        track_shairing_hits.append(track2)

            if(len(track_shairing_hits)>1):
  
                chi2_gttf_array= chi2[indices_for_current_track] ## array for storing track quality of each track
                filtered_track_element = track_shairing_hits[np.argmin(chi2_gttf_array)]
                filtered_tracks.append(filtered_track_element.tolist()) 
                filtered_chi2.append(np.min(chi2_gttf_array))      

            
            else:
                filtered_tracks.append(track.tolist())
                filtered_chi2.append(chi2[iter])

    return filtered_tracks ,np.array(filtered_chi2) 
            
            