# random_sum.py

import numpy as np
import pandas as pd 
import plotly.express as px
import json
from scipy.stats import norm

##barrel and end caps for trackml dataset
barrel = [8, 13, 17]
end_caps = [7, 9, 12, 14, 16, 18]
directory  = "/mnt/data0/Trackml_dataset_100_events/Example_3/feature_store/trainset/event0000210"


def read_files(event_number):
    
    event = directory+str(event_number).zfill(2)
    hits_df = pd.read_csv(event+"-truth.csv")
    particles_df = pd.read_csv(event+"-particles.csv")
    hits_df['r']  = np.sqrt(hits_df.x**2+hits_df.y**2)  ##adding r and phi in the dataframe
    hits_df['phi'] = np.arctan2(hits_df.y,hits_df.x)
    return hits_df,particles_df


def get_signal(event_df,particles_df,config): ##in gev : #return where hits belong to particles that have momentum 1 GeV
    
    barrel_data = {'barrel_layers': [(8, 2), (8, 4), (8, 6), (8, 8), (13, 2), (13, 4), (13, 6), (13, 8), (17, 2), (17, 4)]} 
    values_to_check = np.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9.])
    
    rcut_df = particles_df[(particles_df.vx**2+particles_df.vy**2)**0.5<=config['r_cut']]
    zcut_df = rcut_df[abs(rcut_df.vz)<=config['z0_cut']]
    highpt_pids =  zcut_df[(zcut_df.pt>=config['pt_cut']) ]["particle_id"]

    ##now we have pids that corresponds to particle having pt>=pt and deposit nhits or more
    barrel_df  = load_barrel_df(event_df)
    barrel_df = barrel_df[barrel_df.particle_id.isin(highpt_pids)]

    barrel_hdf = add_barrel_numbers(barrel_df,barrel_data)
    unique_pids = np.unique(barrel_hdf['particle_id'].values)
    unique_pids = unique_pids[unique_pids!=0]
    filtered_pids = []
    for pid in unique_pids:
        df = barrel_hdf[barrel_hdf['particle_id']==pid]
        if len(df['barrel_layers'].unique())>=config['nhits']:
            filtered_pids.append(pid)

    
    signal_df = barrel_hdf[barrel_hdf['particle_id'].isin(filtered_pids)]
    return signal_df

def load_barrel_df(event_df):  
    return event_df[event_df.volume_id.isin(barrel)].reset_index(drop = True)  ##selecting barrel dtaa 



def get_layer_df(df,ids):  ##ids is touple of volume id and layer id (8,2) etc
    vid = ids[0]
    lid = ids[1]
    return df[(df['volume_id']==vid) &(df['layer_id']==lid)]



def plotscatter(dataframe,column_name = None,size = 3):
    fig = px.scatter_3d(dataframe, x='z', y='x', z='y',color = column_name)
    fig.update_traces(marker=dict(size=size))
    fig.update_layout(width=900, height=700)
    return fig


def add_barrel_numbers(df,barrel_data):
    '''add numeric to the barrel layers .like (8,2) - 0, (8,4)- 1 etc'''
    iter  = 0

    for tup in barrel_data['barrel_layers']:
        vid = tup[0]
        lid = tup[1]
        df.loc[(df['volume_id']==vid) &(df['layer_id']==lid), 'barrel_layers'] = iter
        iter+=1
        
    return df.reset_index(drop=True)


###saving files in json format

def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))


def save_dict_as_json(dict,json_file):
    json_str = json.dumps(dict, default=numpy_encoder)
    with open(json_file, 'w') as f:
        f.write(json_str)


def fit_gaussian_and_calculate_sigma(data,config):

    # Fit a Gaussian distribution to the data
    mu, sigma = norm.fit(data)
    
    # Calculate the 3-sigma range

    
    return mu,sigma

def get_layer_wise_data(files,config):
    
    for file in files:
        window = []
        
        json_file = "control_data/layer_wise_" + file + ".json"
        with open (json_file) as f:
            metric = json.load(f)
        if(file=='z0'):
            sigma_cut = config['sigma_cut_doublets']    
        elif(file=='dtheta'):
        
            sigma_cut = config['sigma_cut_triplets'][0]
        else:
            sigma_cut = config['sigma_cut_triplets'][1]        

        for key, values in metric.items():
            mu,sigma = norm.fit(values)
            
            lower_bound = mu -  sigma_cut*sigma
            upper_bound = mu +  sigma_cut*sigma


            window.append((lower_bound,upper_bound))

        config[file] = window