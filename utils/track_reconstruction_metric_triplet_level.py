from .cellular_automata import *
import numpy as np
import pandas as pd
from .global_fit import GTTF

'''
This python function does the following -

1. get triplet array and triplet df - which is after the chi2 metrix has been calculated
2. it also gets signal_df and config
3. The code will perform chi2 cut with triplets
4. build cellular automata tracks and trim it from both ends
5. calculate final track reconstruction efficiency and purity.
6. all this metricx will be stored ion the CA class

'''
def track_reco_meric(track_collection,triplet_arr,triplet_df,hits_df,signal_df):
    '''
    This function calculates the efficiency and purity of the track reconstruction
    not only that it also gives you the track collection in the form of hits
    which might be required for the later purposes
   
    '''    
    reconstructed_pids_50 = []
    reconstructed_pids_100  =  []

    track_collection_hits = []

    for track in track_collection:  ##looping pover final tracks
        hits = []  ##this will store hit ids for all hits within a single track
        for iter,element in enumerate(track):
            iter  = 7-iter
            index_layer = np.where(triplet_df == iter)[0]

            triplet_element = triplet_arr[index_layer][element]
            hits.extend(triplet_element.hitids)

        hits = np.unique(hits)  ##finsiding unique hits..beacuse a hit can be shared by multiple triplets
        track_collection_hits.append(list(hits))
        pid_ = hits_df[hits_df.hit_id.isin(hits)]['particle_id'].values  ##finsding particle id for all hits in a track
        unique, counts = np.unique(pid_, return_counts=True)
        if(np.max(counts)>=5):  ##50 % matching 
            most_common_elements = unique[np.argmax(counts)]    ##finding the most common particle id in a track
            if most_common_elements not in reconstructed_pids_50: ##if the particle id is not already reconstructed
                reconstructed_pids_50.append(most_common_elements)  ##append it to the reconstructed pids
            if(np.max(counts)==10):  ##criterion for 100% matching 
                if most_common_elements not in reconstructed_pids_100:
                    reconstructed_pids_100.append(most_common_elements)  ##append it to the reconstructed pids                

    ##finding the true pids in the signal_df

    signal_pids = signal_df.particle_id.unique()
    
    
    reconstructed_signal_50 = 0
    for pid in reconstructed_pids_50:
        if pid in signal_pids:
            reconstructed_signal_50+=1

    ##for perfect matching efficiency 
    reconstructed_signal_100 = 0
    for pid in reconstructed_pids_100:
        if pid in signal_pids:
            reconstructed_signal_100+=1


    perfect_eff = reconstructed_signal_100/len(signal_pids)
    eff = reconstructed_signal_50/len(signal_pids)
    pur = reconstructed_signal_50/len(track_collection)



    return eff,pur,perfect_eff,track_collection_hits


def build_graph_after_chi2_cut(triplet_arr,triplet_df,config):
    removeable_index = []

    for iter in range(len(triplet_arr)):
        triplet_element=  triplet_arr[iter]
        if(triplet_element.chi2min_gen>config['chi_square_cut']):
                removeable_index.append(iter)
    
    ###filter triplet array and triplet df
    filtered_triplet_arr = np.delete(triplet_arr,removeable_index)
    filtered_triplet_df = np.delete(triplet_df,removeable_index)

    ca_after_chi2_cut = build_graph(filtered_triplet_arr,filtered_triplet_df) 

    return ca_after_chi2_cut,filtered_triplet_arr,filtered_triplet_df








def disconnecting_the_graph(track_collection,chi2_global,label,end):


    '''
    end specifies the region where you wanna trim the graph
    -1 means it trims from first region of triplets(layer 1,2,3)
    zero means it trims from last region of triplets(layer 8,9,10)
    '''
    track_collection = np.array(track_collection)

    node_elements = track_collection[:,end] ###starting from region seven or region one
    unique_elements = np.unique(node_elements)

    track_after_trimming = []
    chi2_after_trimming = []
    label_after_trimming = []
    label = np.array(label)

    for first_node in unique_elements:
        track_tree = track_collection[np.where(track_collection[:,end]== first_node)[0],:]

        ##only one of them will get selected 
        chi2_gttf_array= chi2_global[np.where(track_collection[:,end]== first_node)[0]] ## array for storing track quality of each track
        label_gttf_array = label[np.where(track_collection[:,end]== first_node)[0]] ## array for storing track quality of each track


        filtered_track = track_tree[np.argmin(chi2_gttf_array)]
        track_after_trimming.append(filtered_track.tolist())
        chi2_after_trimming.append(np.min(chi2_gttf_array))
        label_after_trimming.append(label_gttf_array[np.argmin(chi2_gttf_array)])

    return track_after_trimming,np.array(chi2_after_trimming),label_after_trimming




def track_sharing_triplet(track_collection,chi2):
    '''
    if two or many tracks share one triplet, then track with least chi2_glob is selected
    '''
    track_collection = np.array(track_collection)
    filtered_tracks = []
    filtered_chi2 = []
    index_tracks = []


    for iter in range(len(track_collection)):

        if(iter not in index_tracks):
            index_tracks.append(iter)
                
            track = track_collection[iter]
            chi2_sharing_triplets = [chi2[iter]]
            track_shairing_triplets = [track]
            ##find other tracks that share 50 % of the hits
            for iter2 in range(iter+1,len(track_collection)):
                if(iter2 not in index_tracks):

                    track2 = track_collection[iter2]
                    
                    common_triplets = np.sum(np.equal(track,track2))
                    if(common_triplets>=1):
                        index_tracks.append(iter2)
                        track_shairing_triplets.append(track2)
                        chi2_sharing_triplets.append(chi2[iter2])

            if(len(track_shairing_triplets)>1):
  
                
                
                filtered_track_element = track_shairing_triplets[np.argmin(chi2_sharing_triplets)]
               
                filtered_tracks.append(filtered_track_element.tolist()) 
                filtered_chi2.append(np.min(chi2_sharing_triplets))         
            
            else:
                filtered_tracks.append(track)
                filtered_chi2.append(chi2[iter])

    return filtered_tracks,filtered_chi2    



def triplet_array_after_chi2_cut(triplet_arr, triplet_df,config):
    removeable_index = []

    for iter in range(len(triplet_arr)):
        triplet_element=  triplet_arr[iter]
        if(triplet_element.chi2min_gen>config['chi_square_cut']):
                removeable_index.append(iter)
    
    ###filter triplet array and triplet df
    filtered_triplet_arr = np.delete(triplet_arr,removeable_index)
    filtered_triplet_df = np.delete(triplet_df,removeable_index)
    
    return np.array(filtered_triplet_arr),np.array(filtered_triplet_df)



