# Introduction 



This is a Python based pipeline that builds tracks for the TrackML dataset. The project was built in order to complete master thesis at physikalisches institut, Heidelberg University. The pipeline was based on track finding with celllular automata, that was designed to track specific tracks in the barrel region.