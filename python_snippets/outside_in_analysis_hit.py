from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric_triplet_level  import*
from utils.track_reconstruction_metric_hit_level  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*
from utils.global_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)

layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)


final_track_metric = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}

def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/final_config/'

    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = stage_dir+f'ca_class/chi2_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname,ca_fname


class generated_tracks:
    pass



metric_after_first_trim = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}
metric_after_second_trim = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}
final_metric_hit = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}

chi2_dataframe = pd.DataFrame(columns=['event_no','chi2_global','signal_label'])
base = "generated_tracks/hit_level/outside_in/"

for eno in tqdm(range(90)):


    track_class = generated_tracks()
    class_name = base + f'event_{eno}.pkl'

    hdf,pdf = read_files(eno)
    hits_df = load_barrel_df(hdf)
    signal_df = get_signal(hdf,pdf,config)

    triplet_arr_fname,triplet_df_fname,ca_fname = fname(config['sigma_cut_doublets'],eno)



    with open(ca_fname,'rb') as f:
        ca_class = pickle.load(f)    

    track_after_first_trimming,chi2_global_after_first_trimming =  disconnecting_the_graph_hit_level(ca_class.track_collection_hitids,ca_class.chi2_global,0)
                                                                                                     

    track_class.track_after_first_trimming = track_after_first_trimming
    track_class.chi2_after_first_trimming = chi2_global_after_first_trimming

    eff,pur,perfect_eff = track_metric_hit_level(track_after_first_trimming,hits_df,signal_df)

    # print("metric after first trimming")
    # print(eff,pur,perfect_eff)
    metric_after_first_trim['eff'].append(eff)
    metric_after_first_trim['pur'].append(pur)
    metric_after_first_trim['total_tracks'].append(len(track_after_first_trimming))
    metric_after_first_trim['perfect_eff'].append(perfect_eff)

    track_after_second_trimming,chi2_global_after_second_trimming =  disconnecting_the_graph_hit_level(track_after_first_trimming,chi2_global_after_first_trimming,-1)

    track_class.track_after_second_trimming = track_after_second_trimming
    track_class.chi2_after_second_trimming = chi2_global_after_second_trimming

    eff,pur,perfect_eff = track_metric_hit_level(track_after_second_trimming,hits_df,signal_df)

    # print("metric after second trimming")
    # print(eff,pur,perfect_eff)
    metric_after_second_trim['eff'].append(eff)
    metric_after_second_trim['pur'].append(pur)
    metric_after_second_trim['total_tracks'].append(len(track_after_second_trimming))
    metric_after_second_trim['perfect_eff'].append(perfect_eff)


   

    final_track_hit,final_chi2_hit = track_sharing_hits(track_after_second_trimming,chi2_global_after_second_trimming)

    eff,pur,per_eff = track_metric_hit_level(final_track_hit,hits_df,signal_df)

    track_class.final_track_hit = final_track_hit
    track_class.final_chi2_hit = final_chi2_hit

    # print("final track metric at the hit level")
    # print(eff,pur,per_eff)

    final_metric_hit['eff'].append(eff)
    final_metric_hit['pur'].append(pur)
    final_metric_hit['total_tracks'].append(len(final_track_hit))
    final_metric_hit['perfect_eff'].append(perfect_eff)

    label = []

    signal_pids = signal_df.particle_id.unique()
    for track in final_track_hit:
        label.append(is_track_signal(track,signal_pids,hits_df ))
    ##add label, final_chi2_hit and event to the dataframe
    chi2_df = pd.DataFrame({'event_no':[eno]*len(label),'chi2_global':final_chi2_hit,'signal_label':label})
    chi2_dataframe = pd.concat([chi2_dataframe,chi2_df])


    with open(class_name,'wb') as f:
        pickle.dump(track_class,f)


##save the chi2_dataframe
base = "generated_tracks/hit_level/outside_in/"

## Save the chi2_dataframe
chi2_dataframe.to_csv(f'{base}chi2_dataframe.csv', index=False)

## Saving all metrics from previous steps
metric_after_first_trim_df = pd.DataFrame(metric_after_first_trim)
metric_after_second_trim_df = pd.DataFrame(metric_after_second_trim)
final_metric_hit_df = pd.DataFrame(final_metric_hit)

metric_after_first_trim_df.to_csv(f'{base}metric_after_first_trim.csv', index=False)
metric_after_second_trim_df.to_csv(f'{base}metric_after_second_trim.csv', index=False)
final_metric_hit_df.to_csv(f'{base}final_metric_hit.csv', index=False)
