from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.triplet_fit import*
from utils.cellular_automata import*
pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)

layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)


true_triplet_analysis = {'chi_square_min' : [],
                         'curvature' : [],
                         'sigma_curvature' : [],
                         'sigma_ms' : [],
                         }


for eno in tqdm(range(90)):
    
    hdf,pdf = read_files(eno)
    signal_df = get_signal(hdf,pdf,config)
    
    upids = signal_df['particle_id'].unique()
    for pid in upids: 

        df = signal_df[signal_df['particle_id']==pid]
        for layer in range(8):
            hit_a = df[df.barrel_layers==layer]['hit_id'].values
            hit_b = df[df.barrel_layers==layer+1]['hit_id'].values
            hit_c = df[df.barrel_layers==layer+2]['hit_id'].values
            for a in hit_a:
                for b in hit_b:
                    for c in hit_c:
                        trip = triplet([a,b,c],0,0,0)
                        main_triplet_fit(trip,signal_df,config)
                        

                        ###appending matrix
                        true_triplet_analysis["chi_square_min"].append(trip.chi2min_gen)
                        true_triplet_analysis["curvature"].append(trip.C3D_gen)
                        true_triplet_analysis["sigma_curvature"].append(trip.sigmaC3D_gen)
                        true_triplet_analysis["sigma_ms"].append(trip.sigmaMS_gen)
json_file = 'triplet_chi_square.json'
def numpy_int64_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(true_triplet_analysis, default=numpy_int64_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)