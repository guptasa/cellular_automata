from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)
config['sigma_cut'] = 3
layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)




##############################################################

def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/final_config/'

    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = stage_dir+f'ca_class/chi2_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname,ca_fname



def check_key_from_pdg_id(pdg_id):
    if (pdg_id in [-11,11]):
        return 'electron_array'
    elif (pdg_id in [-211,211]):
        return 'pion_array'
    elif (pdg_id in [-321,321]):
        return 'kaon_array'
    elif (pdg_id in [-2212,2212]):
        return 'proton_array'
    elif (pdg_id in [-13,13]):
        return 'muon_array'

pdg_wise_track_eff = {key:[] for key in pdg_dict.keys()}

for eno in tqdm(range(90)):

    particle_counter = {key:0 for key in pdg_dict.keys()}

    triplet_arr_fname,triplet_df_fname,ca_fname = fname(4,eno)
    hdf,pdf = read_files(eno)

    signal_df = get_signal(hdf,pdf,config)

    if(config['signal']):
        hits_df = signal_df
    else:
        hits_df = load_barrel_df(hdf)


    with open(triplet_arr_fname,'rb') as f:
        triplet_arr = pickle.load(f)
    with open(triplet_df_fname,'rb') as f:
        triplet_df = pickle.load(f)
    with open(ca_fname,'rb') as f:
        ca = pickle.load(f)        
    removeable_index = []

    for iter in range(len(triplet_arr)):
        triplet_element=  triplet_arr[iter]
        if(triplet_element.chi2min_gen>config['chi_square_cut']):
                removeable_index.append(iter)

    ###filter triplet array and triplet df
    filtered_triplet_arr = np.delete(triplet_arr,removeable_index)
    filtered_triplet_df = np.delete(triplet_df,removeable_index)
    track_collection  =  ca.track_collection


    reconstructed_pids = []

    track_collection_hits = []

    for track in track_collection:  ##looping pover final tracks
        hits = []  ##this will store hit ids for all hits within a single track
        for iter,element in enumerate(track):
            iter  = 7-iter
            index_layer = np.where(filtered_triplet_df == iter)[0]

            triplet_element = filtered_triplet_arr[index_layer][element]
            hits.extend(triplet_element.hitids)

        hits = np.unique(hits)  ##finsiding unique hits..beacuse a hit can be shared by multiple triplets
        track_collection_hits.append(list(hits))
        pid_ = hits_df[hits_df.hit_id.isin(hits)]['particle_id'].values  ##finsding particle id for all hits in a track
        unique, counts = np.unique(pid_, return_counts=True)
        most_common_elements = unique[np.argmax(counts)]    ##finding the most common particle id in a track
        if most_common_elements not in reconstructed_pids: ##if the particle id is not already reconstructed
            reconstructed_pids.append(most_common_elements)  ##append it to the reconstructed pids


    signal_pids = signal_df.particle_id.unique()
        

    reconstructed_signal = 0
    for pid in reconstructed_pids:
        if pid in signal_pids:
            particle_type = pdf[pdf.particle_id == pid]['particle_type'].values[0]
            key = check_key_from_pdg_id(particle_type)
            particle_counter[key]+=1 

    signal_pids = signal_df.particle_id.unique()
    for key,value in pdg_dict.items():
        particle_signal_df = pdf[pdf.particle_id.isin(signal_pids)]
        pdg_pdf = particle_signal_df[particle_signal_df['particle_type'].isin(value)]
        if(len(pdg_pdf)>0):
            eff = particle_counter[key]/len(pdg_pdf)
            pdg_wise_track_eff[key].append(eff)        

