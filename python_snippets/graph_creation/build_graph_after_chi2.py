from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)

layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)


def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/final_config/'

    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = stage_dir+f'ca_class/chi2_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'

    return triplet_arr_fname,triplet_df_fname,ca_fname


sigma_cut = config['doublet_cut_sigma']
for eno in tqdm(range(90)):

    hdf,pdf = read_files(eno)

    signal_df = get_signal(hdf,pdf,config)

    if(config['signal']):
        hits_df = signal_df
    else:
        hits_df = load_barrel_df(hdf)




    triplet_arr_fname,triplet_df_fname,ca_name_save = fname(sigma_cut,eno)
    
    with open(triplet_arr_fname,'rb') as f:
        triplet_arr = pickle.load(f)
    with open(triplet_df_fname,'rb') as f:
        triplet_df = pickle.load(f)
    
    
    ca_after_chi2,filtered_triplet_arr,filtered_triplet_df = build_graph_after_chi2_cut(triplet_arr,triplet_df,config)

    ##save ca class
    with open(ca_name_save,'wb') as f:
        pickle.dump(ca_after_chi2,f)