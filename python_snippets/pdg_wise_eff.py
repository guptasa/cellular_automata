from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
import yaml
import json
from tqdm import tqdm
import pickle

track_segments = 'triplets'

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)




stage_dir = f'files/doublets'

def fname(stage_dir,eno,signal,z0,pt):
    doublet_fname = stage_dir+f'_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname



with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)

config['pt'] = 0.9
config['z0'] = 225
barrel_data = get_barrel_data(config)

dtheta_array = [0.05,0.055,0.06,0.07]

for dt in dtheta_array:
    config['dtheta'] = dt
    pdg_efficiency = {key: [] for key in pdg_dict} 

    for i in tqdm(range(90)):
        hdf,pdf = read_files(i)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)

        doublet_file = fname(stage_dir,i,config['signal'],config['z0'],config['pt'])



        with open(doublet_file, 'rb') as f:
            doublet_array = pickle.load(f)

        triplet_array,triplet_layer,triplet_pdg = make_triplets_with_pdg(doublet_array,
                                                                    signal_df,
                                                                    hits_df,pdf,config,
                                                                    barrel_data)
        triplet_pdg = np.array(triplet_pdg)


        for key,values in pdg_dict.items():
            constructed_triplets_pdg_truth = len(np.where(triplet_pdg == values[0])[0])+len(np.where(triplet_pdg == values[1])[0])

            hit_df_pdg,_ = get_hitsdf_for_pdgid(signal_df,pdf,values)
            triplets_for_pdgs = calculate_signal_triplets(hit_df_pdg)
            if(triplets_for_pdgs):
                eff = constructed_triplets_pdg_truth/triplets_for_pdgs

                pdg_efficiency[key].append(eff)
    pdg_eff_fname = f"{config['dtheta']}_pdg_eff.pkl"
    with open(pdg_eff_fname,'wb') as f:
        pickle.dump(pdg_efficiency,f)            














































# from utils.readfile import*
# from utils.barrel_data import*
# from utils.doublet import*
# from utils.triplet import*
# from utils.plotting_utils import*
# from importlib import reload
# import yaml
# import json
# from tqdm import tqdm
# import pickle

# with open('files/pdgids.pkl','rb') as f:
#     pdg_dict = pickle.load(f)


# pdg_eff_metric  = {key: [] for key in pdg_dict}  ##calculates efficiency for each particle type for all events in arrays

# def find_key_inpdgdict(pdgid):
#     for key in pdg_dict:
#         if pdgid in pdg_dict[key]:
#             return key
#     return None

# track_segments = 'doublets'
# stage_dir = f'files/doublets'

# with open("config.yaml", "r") as f:
#     config = yaml.safe_load(f)


# barrel_data = get_barrel_data(config)

# for i in tqdm(range(90)):
#     constructed_triplet_truth = {key: 0 for key in pdg_dict}  ##calculates constructed true triplets for each particle type for one event

#     hdf,pdf = read_files(i)
#     signal_df = get_signal(hdf,pdf,config)
#     signal_df = add_barrel_numbers(signal_df,barrel_data)
#     hits_df = load_barrel_df(hdf)
    
#     triplet_array_name = f'/mnt/data1/gupta/ca/triplets/event_{i}_triplet_array.pkl'
#     triplet_df_name = f'/mnt/data1/gupta/ca/triplets/event_{i}_triplet_df.pkl'

#     with open(triplet_array_name, 'rb') as f:
#         triplet_array = pickle.load(f)
#     with open(triplet_df_name, 'rb') as f:
#         triplet_layer = pickle.load(f)    
   
#     for trip in triplet_array:
#         if(trip.label):
#             particle_id = hits_df.loc[hits_df.hit_id==trip.hitids[2]]['particle_id'].values[0]
#             pdg_id = pdf.loc[pdf.particle_id==particle_id]['particle_type'].values[0]
#             key = find_key_inpdgdict(pdg_id)
#             if(key):
#                 constructed_triplet_truth[key] += 1
    
#     ##append the final metric with the efficiency 
#     for key,values in pdg_dict.items():


#         hit_df_pdg,_ = get_hitsdf_for_pdgid(signal_df,pdf,values)
#         triplets_for_pdgs = calculate_signal_triplets(hit_df_pdg)
#         if(triplets_for_pdgs):
#             eff = constructed_triplet_truth[key]/triplets_for_pdgs
#             pdg_eff_metric[key].append(eff)
            
# pdg_eff_fname = f"dt_{config['dtheta']}_dk_{config['dkappa']}pdg_wise_eff.pkl"
# with open(pdg_eff_fname,'wb') as f:
#     pickle.dump(pdg_eff_metric,f) 


                
       
