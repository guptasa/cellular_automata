from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric_triplet_level  import*
from utils.track_reconstruction_metric_hit_level  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*
class generated_tracks:
    pass

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)
config['sigma_cut'] = 3
layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)
def fname(eno):


    triplet_arr_fname  = f'generated_tracks/triplets/chi2_7_triplet_arr{eno}.pkl'
    triplet_df_fname  = f'generated_tracks/triplets/chi2_7_triplet_df{eno}.pkl'
    ca_fname = f'generated_tracks/ca_class_eno_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname,ca_fname


def load_triplets(eno):
    triplet_arr_fname,triplet_df_fname,_ = fname(eno)
    with open(triplet_arr_fname,'rb') as f:
        triplet_arr = pickle.load(f)
    with open(triplet_df_fname,'rb') as f:
        triplet_df = pickle.load(f)
    config['chi_square_cut'] = 6
    return triplet_array_after_chi2_cut(triplet_arr,triplet_df,config)    



def load_ca(eno):
    _,_,ca_fname = fname(eno)
    with open(ca_fname,'rb') as f:
        ca = pickle.load(f)
    return ca



metric_after_ptc2_cut = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}
metric_after_first_trim = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}
metric_after_second_trim = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}
final_metric_hit = {'eff':[],'pur':[],'total_tracks':[],'perfect_eff':[]}

# chi2_dataframe = pd.DataFrame(columns=['event_no','chi2_global','signal_label'])
base = "generated_tracks/triplet_level/inside_out/"

for eno in tqdm(range(90)):


    track_class = generated_tracks()
    class_name = base + f'event_{eno}.pkl'

    hdf,pdf = read_files(eno)
    hits_df = load_barrel_df(hdf)
    signal_df = get_signal(hdf,pdf,config)

    ca_class = load_ca(eno)
    filtered_triplet_arr,filtered_triplet_df = load_triplets(eno)

    
    ###############################################################################
    label_after_ptc2_cut = []
    chi2_after_ptc2_cut = []
    index_after_ptc2_cut = []
    for i in range(len(ca_class.label)):
        if(ca_class.pt[i]>=1000 and ca_class.chi2_gttf_array[i]<45):
            index_after_ptc2_cut.append(i)
            label_after_ptc2_cut.append(ca_class.label[i])
            chi2_after_ptc2_cut.append(ca_class.chi2_gttf_array[i])
    
    tracks_after_ptc2_cut = []

    for iter in index_after_ptc2_cut:
        tracks_after_ptc2_cut.append(ca_class.track_collection[iter])

    track_class.label_after_ptc2_cut = label_after_ptc2_cut
    track_class.chi2_after_ptc2_cut = chi2_after_ptc2_cut
    track_class.tracks_after_ptc2_cut = tracks_after_ptc2_cut

    eff,pur,per_eff,track_collection_hits = track_reco_meric(tracks_after_ptc2_cut,
                 filtered_triplet_arr,
                 filtered_triplet_df,
                 hits_df,
                 signal_df)
##################################################################################
    metric_after_ptc2_cut['eff'].append(eff)
    metric_after_ptc2_cut['pur'].append(pur)
    metric_after_ptc2_cut['total_tracks'].append(len(tracks_after_ptc2_cut))
    metric_after_ptc2_cut['perfect_eff'].append(per_eff)

    



    ##################################################################################

    track_after_first_trimming,chi2_global_after_first_trimming,label_after_first_trimming =  disconnecting_the_graph(tracks_after_ptc2_cut,np.array(chi2_after_ptc2_cut),label_after_ptc2_cut,-1)
    track_class.track_after_first_trimming = track_after_first_trimming
    track_class.chi2_after_first_trimming = chi2_global_after_first_trimming
    track_class.label_after_first_trimming = label_after_first_trimming

    eff,pur,perfect_eff,total_tracks = track_reco_meric(track_after_first_trimming,filtered_triplet_arr,
                                                        filtered_triplet_df,hits_df,signal_df)

    # print("metric after first trimming")
    # print(eff,pur,perfect_eff)
    metric_after_first_trim['eff'].append(eff)
    metric_after_first_trim['pur'].append(pur)
    metric_after_first_trim['total_tracks'].append(len(track_after_first_trimming))
    metric_after_first_trim['perfect_eff'].append(perfect_eff)

    ####################################################################################################



    track_after_second_trimming,chi2_global_after_second_trimming,label_after_second_trimming =  disconnecting_the_graph(track_after_first_trimming,np.array(chi2_global_after_first_trimming),label_after_first_trimming,0)
    track_class.track_after_second_trimming = track_after_second_trimming
    track_class.chi2_after_second_trimming = chi2_global_after_second_trimming

    eff,pur,perfect_eff,total_tracks = track_reco_meric(track_after_second_trimming,filtered_triplet_arr,
                                                        filtered_triplet_df,hits_df,signal_df)

    # print("metric after second trimming")
    # print(eff,pur,perfect_eff)
    metric_after_second_trim['eff'].append(eff)
    metric_after_second_trim['pur'].append(pur)
    metric_after_second_trim['total_tracks'].append(len(track_after_second_trimming))
    metric_after_second_trim['perfect_eff'].append(perfect_eff)


    ##############################################################################################



    final_track_hit,final_chi2_hit = track_sharing_hits(total_tracks,np.array(chi2_global_after_second_trimming))
    eff,pur,per_eff = track_metric_hit_level(final_track_hit,hits_df,signal_df)


    track_class.final_track_hit = final_track_hit
    track_class.final_chi2_hit = final_chi2_hit

    # print("final track metric at the hit level")
    # print(eff,pur,per_eff)

    final_metric_hit['eff'].append(eff)
    final_metric_hit['pur'].append(pur)
    final_metric_hit['total_tracks'].append(len(final_track_hit))
    final_metric_hit['perfect_eff'].append(perfect_eff)

   

    with open(class_name,'wb') as f:
        pickle.dump(track_class,f)


##save the chi2_dataframe

##saving all metrics from preveious steps
metric_after_ptc2_cut_df = pd.DataFrame(metric_after_ptc2_cut)
metric_after_first_trim_df = pd.DataFrame(metric_after_first_trim)
metric_after_second_trim_df = pd.DataFrame(metric_after_second_trim)
final_metric_hit_df = pd.DataFrame(final_metric_hit)

metric_after_ptc2_cut_df.to_csv('generated_tracks/triplet_level/inside_out/metric_after_ptc2_cut.csv',index=False)
metric_after_first_trim_df.to_csv('generated_tracks/triplet_level/inside_out/metric_after_first_trim.csv',index=False)
metric_after_second_trim_df.to_csv('generated_tracks/triplet_level/inside_out/metric_after_second_trim.csv',index=False)
final_metric_hit_df.to_csv('generated_tracks/triplet_level/inside_out/final_metric_hit.csv',index=False)