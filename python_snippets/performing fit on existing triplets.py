from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.triplet_fit import*
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
'''
This program does the following - Load triplets
perform fit
save triplets again
built graph ca with filtered triplets
save the ca graph again with different name
'''






pd.set_option('display.max_columns', None) 
def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)

sigma_cut_arr = [3,4,5,6]

def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/'

    doublet_fname = stage_dir+f'doublets/{eno}.pkl'
    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = stage_dir+f'ca_class/after_chi2_cut_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname,triplet_arr_fname,triplet_df_fname,ca_fname





for sigma_cut in sigma_cut_arr:
    print("--------------------------------------------------------------------------------------------------------------------")
    print("--------------------------------------------------------------------------------------------------------------------")

    print(f"--------------------------------------------{sigma_cut}------------------------------------------------------------")
    print("--------------------------------------------------------------------------------------------------------------------")
    print("--------------------------------------------------------------------------------------------------------------------")




    triplet_metrics_after_fitting = {'eff':np.zeros((90)),
                    'pur':np.zeros((90)),
                    'total_segments':np.zeros((90)),
                    'signal_segments':np.zeros((90))}

    config['sigma_cut'] = sigma_cut
    layer_files = ['z0','dtheta','dkappa']
    get_layer_wise_data(layer_files,config)



    for i in tqdm(range(90)):


                    
        hdf,pdf = read_files(i)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)

        _,triplet_arr_fname,triplet_df_fname,ca_fname = fname(sigma_cut,i)


        with open(triplet_arr_fname,'rb') as f:
            triplet_arr = pickle.load(f)
        with open(triplet_df_fname,'rb') as f:
            triplet_df = pickle.load(f)

        index_array = []
        for iter in range(len(triplet_arr)):
            triplet_element = triplet_arr[iter]
            main_triplet_fit(triplet_element,hits_df,config)
            ###perform chi squre cut
            if(triplet_element.chi2min_gen<config['chi_square_cut']):
                index_array.append(iter)

        ##save triplets again with the same fname

        with open(triplet_arr_fname, 'wb') as f:
            pickle.dump(triplet_arr, f)
        with open(triplet_df_fname, 'wb') as f:
            pickle.dump(triplet_df, f)

        ###ffiltering triplets
        filtered_triplet_arr = [triplet_arr[index] for index in index_array]
        filtered_triplet_df = [triplet_df[index] for index in index_array]
        ###calculate metrics after fitting                 



        eff,pur,ts,ss = calculate_triplet_metrics(filtered_triplet_arr,signal_df)
        triplet_metrics_after_fitting['eff'][i] = eff
        triplet_metrics_after_fitting['pur'][i] = pur
        triplet_metrics_after_fitting['total_segments'][i] = ts
        triplet_metrics_after_fitting['signal_segments'][i] = ss

        ###build graph
        ca = build_graph(filtered_triplet_arr,filtered_triplet_df)


        with open(ca_fname, 'wb') as f:
            pickle.dump(ca, f)





    
    triplet_json = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/triplet_metrics_after_chi2cut.json'



    save_dict_as_json(triplet_metrics_after_fitting,triplet_json)

print("finsihed")
