from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)
config['sigma_cut'] = 3
layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)

def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/'
    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'

    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname


sigmas =  [3,4,5,6]

for sigma in sigmas:
    print(sigma)
    metric = {'eff':[],'pur':[]}
    for eno in tqdm(range(90)):

                

        hdf,pdf = read_files(eno)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)


        base_path = '/mnt/data1/gupta/ca/'
        metrics_file_name = f'sigma/ca_class/{eno}.pkl'
        ca_class_name = f'{base_path}{sigma}{metrics_file_name}'
        with open(ca_class_name,'rb') as f:
            ca_class = pickle.load(f)
            


        triplet_arr_fname,triplet_df_fname = fname(sigma,eno)

        with open(triplet_arr_fname,'rb') as f:
            triplet_arr = pickle.load(f)
        with open(triplet_df_fname,'rb') as f:
            triplet_df = pickle.load(f)

        triplet_arr = np.array(triplet_arr)
        triplet_df = np.array(triplet_df)
        track_collection = ca_class.track_collection



        final_tracks = np.array(track_collection)

        # final_tracks = np.array(track_collection)

        reconstructed_pids = []

        for track in final_tracks:  ##looping pover final tracks
            hits = []  ##this will store hit ids for all hits within a single track
            for iter,element in enumerate(track):
                iter  = 7-iter
                index_layer = np.where(triplet_df == iter)[0]

                triplet_element = triplet_arr[index_layer][element]
                hits.extend(triplet_element.hitids)

            hits = np.unique(hits)  ##finsiding unique hits..beacuse a hit can be shared by multiple triplets

            pid_ = hits_df[hits_df.hit_id.isin(hits)]['particle_id'].values  ##finsding particle id for all hits in a track
            unique, counts = np.unique(pid_, return_counts=True)
            most_common_elements = unique[np.argmax(counts)]    ##finding the most common particle id in a track
            if most_common_elements not in reconstructed_pids: ##if the particle id is not already reconstructed
                reconstructed_pids.append(most_common_elements)  ##append it to the reconstructed pids

        ##finding the true pids in the signal_df

        signal_pids = signal_df.particle_id.unique()


        reconstructed_signal = 0
        for pid in reconstructed_pids:
            if pid in signal_pids:
                reconstructed_signal+=1

        eff = reconstructed_signal/len(signal_pids)
        pur = reconstructed_signal/len(final_tracks)
        metric['eff'].append(eff)
        metric['pur'].append(pur)
    file_name = f'track_metric_without_trimming_{sigma}.json'
    save_dict_as_json(metric,file_name)    
        
