from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle


def fname(stage_dir,eno,signal,z0,pt):
    doublet_fname = stage_dir+f'_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname


track_segments = 'doublets'

stage_dir = f'files/doublets'



with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)


barrel_data = get_barrel_data(config)
print("triple_building started")


for i in tqdm(range(90)):
    hdf,pdf = read_files(i)
    signal_df = get_signal(hdf,pdf,config)
    doublet_nam = fname(stage_dir,i,config['signal'],config['z0'],config['pt']) 
    with open(doublet_nam, 'rb') as f:
        doublet_array = pickle.load(f)
    hits_df = load_barrel_df(hdf)    
    triplet_array,triplet_df = make_triplets(doublet_array,signal_df,hits_df,config,barrel_data) 
    triplet_array_name = f'/mnt/data1/gupta/ca/triplets/event_{i}_triplet_array.pkl'
    triplet_df_name = f'/mnt/data1/gupta/ca/triplets/event_{i}_triplet_df.pkl'

    with open(triplet_array_name, 'wb') as f:
        pickle.dump(triplet_array, f)   
    with open(triplet_df_name, 'wb') as f:
        pickle.dump(triplet_df, f)    

print("triplet_building done")