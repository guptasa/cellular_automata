from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)
config['sigma_cut'] = 3
layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)


##all particles together 



def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/final_config/'

    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = stage_dir+f'ca_class/chi2_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname,ca_fname





final_track_metric = {'eff':[],'pur':[],'perfect_eff':[],'total_tracks':[]}
for eno in tqdm(range(90)):
    triplet_arr_fname,triplet_df_fname,ca_fname = fname(4,eno)
    hdf,pdf = read_files(eno)

    signal_df = get_signal(hdf,pdf,config)

    if(config['signal']):
        hits_df = signal_df
    else:
        hits_df = load_barrel_df(hdf)


    with open(triplet_arr_fname,'rb') as f:
        triplet_arr = pickle.load(f)
    with open(triplet_df_fname,'rb') as f:
        triplet_df = pickle.load(f)
    with open(ca_fname,'rb') as f:
        ca = pickle.load(f)        
    removeable_index = []

    for iter in range(len(triplet_arr)):
        triplet_element=  triplet_arr[iter]
        if(triplet_element.chi2min_gen>config['chi_square_cut']):
                removeable_index.append(iter)

    ###filter triplet array and triplet df
    filtered_triplet_arr = np.delete(triplet_arr,removeable_index)
    filtered_triplet_df = np.delete(triplet_df,removeable_index)
    track_collection  =  ca.track_collection


    eff,pur,perfect_eff,track_hits = track_reco_meric(track_collection,filtered_triplet_arr,filtered_triplet_df,hits_df,signal_df)

   
    final_track_metric['eff'].append(eff)
    final_track_metric['pur'].append(pur)
    final_track_metric['total_tracks'].append(len(track_collection))
    final_track_metric['perfect_eff'].append(perfect_eff)

json_fname = 'final_track_metric_after_neighbor_correction.json'
save_dict_as_json(final_track_metric,json_fname)