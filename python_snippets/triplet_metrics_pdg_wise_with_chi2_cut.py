from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)
config['sigma_cut'] = 3
layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)



def check_key_from_pdg_id(pdg_id):
    if (pdg_id in [-11,11]):
        return 'electron_array'
    elif (pdg_id in [-211,211]):
        return 'pion_array'
    elif (pdg_id in [-321,321]):
        return 'kaon_array'
    elif (pdg_id in [-2212,2212]):
        return 'proton_array'
    elif (pdg_id in [-13,13]):
        return 'muon_array'
def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/'
    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'

    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname




for sigma_cut in [3,4,5,6]:
        
    print('####################################')
    print(sigma_cut)
    print('####################################')

    ############################################################# WHat we populate in this code
    pdg_wise_triplet_eff= {key:[] for key in pdg_dict.keys()}
    total_triplets_pdg_wise = {key:0 for key in pdg_dict.keys()} ##total triplets pdg wise for 90 events

    #############################################################

    for eno in tqdm(range(90)):

        hdf,pdf = read_files(eno)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)




        triplet_arr_fname,triplet_df_fname = fname(sigma_cut,eno)

        with open(triplet_arr_fname,'rb') as f:
            triplet_arr = pickle.load(f)
        with open(triplet_df_fname,'rb') as f:
            triplet_df = pickle.load(f)

        triplet_counter_pdg_wise  = {key:0 for key in pdg_dict.keys()}  ##reconstructed triplets 


        for trip in triplet_arr:
            if(trip.label):
                trip_particle_id = hdf[hdf.hit_id.isin(trip.hitids)]['particle_id'].values[0]
                
                trip_pdg_id = pdf[pdf.particle_id == trip_particle_id]['particle_type'].values[0]
                key = check_key_from_pdg_id(trip_pdg_id)
                triplet_counter_pdg_wise[key]+=1



        for key,value in pdg_dict.items():


            total_signal_pids = signal_df['particle_id'].unique()

            ##getting particle type from pdf dataframe
            particle_signal_df = pdf[pdf.particle_id.isin(total_signal_pids)]
            pdg_pdf = particle_signal_df[particle_signal_df['particle_type'].isin(value)]

            pdg_pids = pdg_pdf['particle_id'].unique()

            pdg_df = signal_df[signal_df['particle_id'].isin(pdg_pids)]

            total_pdg_triplets = calculate_signal_triplets(pdg_df)

            if(total_pdg_triplets):
                eff = triplet_counter_pdg_wise[key]/total_pdg_triplets
                pdg_wise_triplet_eff[key].append(eff)
                total_triplets_pdg_wise[key]+=total_pdg_triplets
       
    pdg_wise_triplet_eff_name = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/' + 'pdg_wise_triplet_eff.json'
    save_dict_as_json(pdg_wise_triplet_eff,pdg_wise_triplet_eff_name)
    total_triplets_pdg_wise_name = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/' + 'total_triplets_pdg_wise.json'
    save_dict_as_json(total_triplets_pdg_wise,total_triplets_pdg_wise_name)   


    
