from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
import yaml
import json
from tqdm import tqdm
import pickle




def fname(stage_dir,eno,signal,z0,pt):
    doublet_fname = stage_dir+f'_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname


def create_doublets_ptlim():

    track_segments = 'doublets'
    lim = 'ptlim'
    stage_dir = f'/mnt/data1/gupta/ca/{track_segments}/{lim}/'
    pt_arr = [0.6,0.7,0.8,0.9,1]


    for pt in pt_arr:
        print(pt)


        with open("config.yaml", "r") as f:

            # Load and parse the YAML file
            config = yaml.safe_load(f)

        config['pt'] = pt
        config['z0'] = 250


        barrel_data = get_barrel_data(config)

        for i in tqdm(range(90)):


                        
            hdf,pdf = read_files(i)

            signal_df = get_signal(hdf,pdf,config)

            if(config['signal']):
                hits_df = signal_df
            else:
                hits_df = load_barrel_df(hdf)

            doublet_array = make_all_doublets(hits_df,barrel_data,config)
            

            doublet_file = fname(stage_dir,i,config['signal'],config['z0'],config['pt'])


            with open(doublet_file, 'wb') as f:
                pickle.dump(doublet_array, f)


    print("finsihed")


def create_doublets_zlim():


    track_segments = 'doublets'
    lim = 'zlim'
    stage_dir = f'/mnt/data1/gupta/ca/{track_segments}/{lim}/'



    Z_arr = [200,225,250,300]

    for z in Z_arr:
        print(z)


        with open("config.yaml", "r") as f:

            # Load and parse the YAML file
            config = yaml.safe_load(f)

        config['pt'] = 0.5
        config['z0'] = z


        barrel_data = get_barrel_data(config)

        for i in tqdm(range(90)):


                        
            hdf,pdf = read_files(i)

            signal_df = get_signal(hdf,pdf,config)

            if(config['signal']):
                hits_df = signal_df
            else:
                hits_df = load_barrel_df(hdf)

            doublet_array = make_all_doublets(hits_df,barrel_data,config)
            

            doublet_file = fname(stage_dir,i,config['signal'],config['z0'],config['pt'])


            with open(doublet_file, 'wb') as f:
                pickle.dump(doublet_array, f)


    print("finsihed")


print("Making ptlim doublets")

create_doublets_ptlim()

print("Making zlim doublets")

create_doublets_zlim()
