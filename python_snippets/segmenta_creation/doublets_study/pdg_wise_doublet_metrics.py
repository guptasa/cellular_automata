# signal: False  ##for building doublets with noise hits or without noise hits
# pt: 0.6  ##this is used for the calculation of dphi for the doublet formation 
# B: 2  ##magnetic field 
# sigma_cut_doublets : 4  ##this is for early stage building of doublets...the distribution is gaussian for dz and the cut is mu-cut*sigma to mu+cut*sigma
# sigma_cut_triplets : 4  ##this is for the final stage building of triplets...the distribution is gaussian for dkappa,d and the cut is mu-cut*sigma to mu+cut*sigma
# chi_square_cut : 7
# material: 0.015  ##this is the material budget for the trackml dataset

# ################# Signal requirements ############################
# pt_cut: 1   ## this is for the trackml dataset ...the cut selects particle 
# nhits: 10  ##minimum layers where particle deposit hit (required for signal calculation)
# z0_cut: 200  ##minimum z0 cut for a high pt particle to deposit in the barrel region
# r_cut: 32.302265119742735 ##minimum r cut for a high pt particle to deposit in the barrel region









from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
pd.set_option('display.max_columns', None) 


def check_key_from_pdg_id(pdg_id):
    if (pdg_id in [-11,11]):
        return 'electron_array'
    elif (pdg_id in [-211,211]):
        return 'pion_array'
    elif (pdg_id in [-321,321]):
        return 'kaon_array'
    elif (pdg_id in [-2212,2212]):
        return 'proton_array'
    elif (pdg_id in [-13,13]):
        return 'muon_array'

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)

barrel_data = get_barrel_data(config)

def load_doublets(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/'

    doublet_fname = stage_dir+f'doublets/{eno}.pkl'

    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    with open(doublet_fname,'rb') as f:
        doublet_arr = pickle.load(f)
    return doublet_arr     


pdg_wise_doublet_eff= {key:[] for key in pdg_dict.keys()}
total_signal_doublets_pdg_wise = {key:0 for key in pdg_dict.keys()} 


for eno in tqdm(range(90)):


    hdf,pdf = read_files(eno)

    signal_df = get_signal(hdf,pdf,config)

    if(config['signal']):
        hits_df = signal_df
    else:
        hits_df = load_barrel_df(hdf)

    doublet_counter_pdg_wise  = {key:0 for key in pdg_dict.keys()}  ##reconstructed doublets 



    doublet_arr = load_doublets(config['sigma_cut_doublets'],eno)


    pid = signal_df.particle_id.unique()
    for i in range(len(doublet_arr)):
        d = doublet_arr[i]
        if(sum(d.labels)):
            if(d.pid1 in pid):
                pdg_id = pdf[pdf.particle_id == d.pid1]['particle_type'].values[0]
                key = check_key_from_pdg_id(pdg_id)
                doublet_counter_pdg_wise[key] += sum(d.labels)



    for key,value in pdg_dict.items():


        total_signal_pids = signal_df['particle_id'].unique()

        ##getting particle type from pdf dataframe
        particle_signal_df = pdf[pdf.particle_id.isin(total_signal_pids)]
        pdg_pdf = particle_signal_df[particle_signal_df['particle_type'].isin(value)]

        pdg_pids = pdg_pdf['particle_id'].unique()

        pdg_df = signal_df[signal_df['particle_id'].isin(pdg_pids)]

        total_pdg_doublets = total_signal_doublets(pdg_df,barrel_data)

        if(total_pdg_doublets):
            eff = doublet_counter_pdg_wise[key]/total_pdg_doublets
            pdg_wise_doublet_eff[key].append(eff)
            total_signal_doublets_pdg_wise[key] += total_pdg_doublets