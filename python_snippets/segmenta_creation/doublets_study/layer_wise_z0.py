
from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from scipy.stats import norm


pd.set_option('display.max_columns', None) 


with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)



barrel_data = get_barrel_data(config)

def z_intersept(z1,r1,z2,r2):
    if (z2-z1) :
            
        m = (r2-r1)/(z2-z1)
        
        c = r1 - m*z1
        return -c/m
    else: return 0


layer_wise_z0 = {i:[] for i in range(9)}
for eno in tqdm(range(90)):
    hdf,pdf = read_files(eno)


    signal_df = get_signal(hdf,pdf,config)
    pids = signal_df.particle_id.unique()

    for pid in pids:
        pdf = signal_df[signal_df.particle_id == pid]
        barrel_info = pdf.barrel_layers.unique()
        for i in range(9):
            if(sum(barrel_info==i) != 0 and sum (barrel_info==i+1) != 0):
                barrel_i = pdf[pdf.barrel_layers == i]
                barrel_i1 = pdf[pdf.barrel_layers == i+1]

                r1 = barrel_i.r.mean()
                z1 = barrel_i.z.mean()

                r2 = barrel_i1.r.mean()
                z2 = barrel_i1.z.mean()

                z0 = z_intersept(z1,r1,z2,r2)
                layer_wise_z0[i].append(z0) 


save_dict_as_json(layer_wise_z0,'control_data/layer_wise_z0.json')
