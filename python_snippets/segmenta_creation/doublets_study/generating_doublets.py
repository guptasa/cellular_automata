from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
pd.set_option('display.max_columns', None) 


with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)

sigma = config['sigma_cut_doublets']
config_fname = f'/mnt/data1/gupta/ca/{sigma}' +'sigma/config.yaml'

##saving the config file there
with open(config_fname, 'w') as f:
    yaml.dump(config, f)



barrel_data = get_barrel_data(config)

print(config)

def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/'

    doublet_fname = stage_dir+f'doublets/{eno}.pkl'

    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname







doublet_metrics = {'eff':np.zeros((90)),
                'pur':np.zeros((90)),
                'total_segments':np.zeros((90)),
                'signal_segments':np.zeros((90))}



layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)



for i in tqdm(range(90)):


                
    hdf,pdf = read_files(i)

    signal_df = get_signal(hdf,pdf,config)

    if(config['signal']):
        hits_df = signal_df
    else:
        hits_df = load_barrel_df(hdf)

    doublet_array = make_all_doublets(hits_df,barrel_data,config)
    doublet_fname = fname(config['sigma_cut_doublets'],i)
    with open(doublet_fname, 'wb') as f:
        pickle.dump(doublet_array, f)
    eff,pur,ts,ss = eff_and_pur(doublet_array,signal_df,barrel_data)
    doublet_metrics['eff'][i] = eff
    doublet_metrics['pur'][i] = pur
    doublet_metrics['total_segments'][i] = ts
    doublet_metrics['signal_segments'][i] = ss







doublet_json = f'/mnt/data1/gupta/ca/{sigma}' +'sigma/doublet_metrics.json'


save_dict_as_json(doublet_metrics,doublet_json)

print("finsihed")
