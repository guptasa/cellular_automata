##from doublet arrayes calculates the metrics

from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
import yaml
import json
import numpy as np
from tqdm import tqdm
import pickle

Z_arr = [150,175,200,225,250,275,300]
pt_arr = [0.5,0.6,0.7,0.8,0.9,1]

track_segments = 'doublets'
lim = 'zlim'




doublet_metrics = {'eff':np.zeros((90,len(Z_arr))),
                   'pur':np.zeros((90,len(Z_arr))),
                   'total_segments':np.zeros((90,len(Z_arr))),
                   'signal_segments':np.zeros((90,len(Z_arr)))}


stage_dir = f'/mnt/data1/gupta/ca/{track_segments}/{lim}/'

def fname(eno,signal,z0,pt):
    doublet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname,triplet_fname


def update_metrics(metrics, values, eno, zno):
    metrics['eff'][eno, zno] = values[0]
    metrics['pur'][eno, zno] = values[1]
    metrics['total_segments'][eno, zno] = values[2]
    metrics['signal_segments'][eno, zno] = values[3]


def get_fname(eno,signal,z0,pt):
    fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return fname

for iter,z in enumerate(Z_arr):


    with open("config.yaml", "r") as f:

    # Load and parse the YAML file
        config = yaml.safe_load(f)
    config['pt'] = 0.5
    config['z0'] = z
    barrel_data = get_barrel_data(config)    
    
    for i in tqdm(range(90)):

        
        hdf,pdf = read_files(i)
        signal_df = get_signal(hdf,pdf,config)
        doublet_file,_= fname(i,config['signal'],config['z0'],config['pt'])
        

        with open(doublet_file,'rb') as f:
            doublet_array = pickle.load(f)

 
        values = eff_and_pur(doublet_array,signal_df,barrel_data)

        # values = eff_and_pur(doublet_array,signal_df,barrel_data)
        update_metrics(doublet_metrics,values,i,iter)





json_file = f'analysis/{track_segments}_{lim}_metrics.json'
###saving the json file
def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(doublet_metrics, default=numpy_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)


##the resulted metrics can be plotted by box plot functions in plotting_utils