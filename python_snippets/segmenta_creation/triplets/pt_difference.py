from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
import yaml
import json
from tqdm import tqdm
import pickle


print("starting")
def calculate_curvature(x1, y1, x2, y2, x3, y3):
    r12_x = x2 - x1
    r12_y = y2 - y1
    r13_x = x3 - x1
    r13_y = y3 - y1
    r23_x = x3 - x2
    r23_y = y3 - y2
    
    r13_mod = np.sqrt(r13_x**2 + r13_y**2)
    r12_mod = np.sqrt(r12_x**2 + r12_y**2)
    r23_mod = np.sqrt(r23_x**2 + r23_y**2)
    
    num = abs(r23_x * r12_y - r12_x * r23_y)
    
    return 2 * num / (r12_mod * r13_mod * r23_mod)



dpt_dict = {i:[] for i in range(7)}
with open("config.yaml", "r") as f:
    # Load and parse the YAML file
    config = yaml.safe_load(f)

barrel_data = get_barrel_data(config)


for eno in tqdm(range(90)):
    hdf,pdf = read_files(eno)
    signal_df = get_signal(hdf,pdf,config)
    signal_df = add_barrel_numbers(signal_df,barrel_data)

    signal_pids = signal_df.particle_id.unique()
    for pid in signal_pids:
        _df = signal_df[signal_df.particle_id==pid]

        barrel_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        frequency_array = np.array([np.sum(_df['barrel_layers'].values == element) for element in barrel_num])
        for i in range(7):
            triplet_layer = frequency_array[i:i+4]
            all_nonzero = np.all(triplet_layer != 0)
            if all_nonzero:




                xl1 = _df[_df.barrel_layers==barrel_num[i]].x.mean()
                yl1 = _df[_df.barrel_layers==barrel_num[i]].y.mean()

                xl2 = _df[_df.barrel_layers==barrel_num[i+1]].x.mean()
                yl2 = _df[_df.barrel_layers==barrel_num[i+1]].y.mean()

                xl3 = _df[_df.barrel_layers==barrel_num[i+2]].x.mean()
                yl3 = _df[_df.barrel_layers==barrel_num[i+2]].y.mean()

                xl4 = _df[_df.barrel_layers==barrel_num[i+3]].x.mean()
                yl4 = _df[_df.barrel_layers==barrel_num[i+3]].y.mean()
                

                k123 = calculate_curvature(xl1,yl1,xl2,yl2,xl3,yl3)
                k234 = calculate_curvature(xl2,yl2,xl3,yl3,xl4,yl4)

                pt123 = 0.3*config['B']/k123
                pt234 = 0.3*config['B']/k234


                dpt_dict[i].append(pt234-pt123)

file_name = 'dpt.pkl'

with open(file_name,'wb') as f:
    pickle.dump(dpt_dict,f)
print("done")


