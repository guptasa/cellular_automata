from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
import yaml
import json
from tqdm import tqdm
import pickle

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
dtheta_dict = {key: np.array([]) for key in pdg_dict}
with open("config.yaml", "r") as f:
    # Load and parse the YAML file
    config = yaml.safe_load(f)

barrel_data = get_barrel_data(config)


for key,value in pdg_dict.items():
    dtheta_dict[key] = calculate_polar_angle(value,config,barrel_data)


json_file = 'dtheta_metrics.json'
###saving the json file
def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(dtheta_dict, default=numpy_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)


