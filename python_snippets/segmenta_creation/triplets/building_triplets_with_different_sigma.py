from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
pd.set_option('display.max_columns', None) 
def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)

sigma_cut_arr = [3,4,5,6]

def fname(sigma_cut,eno):

    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/'

    doublet_fname = stage_dir+f'doublets/{eno}.pkl'
    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = stage_dir+f'ca_class/{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname,triplet_arr_fname,triplet_df_fname,ca_fname





for sigma_cut in sigma_cut_arr:
    print("--------------------------------------------------------------------------------------------------------------------")
    print("--------------------------------------------------------------------------------------------------------------------")

    print(f"--------------------------------------------{sigma_cut}------------------------------------------------------------")
    print("--------------------------------------------------------------------------------------------------------------------")
    print("--------------------------------------------------------------------------------------------------------------------")


    doublet_metrics = {'eff':np.zeros((90)),
                    'pur':np.zeros((90)),
                    'total_segments':np.zeros((90)),
                    'signal_segments':np.zeros((90))}


    triplet_metrics = {'eff':np.zeros((90)),
                    'pur':np.zeros((90)),
                    'total_segments':np.zeros((90)),
                    'signal_segments':np.zeros((90))}

    config['sigma_cut'] = sigma_cut
    layer_files = ['z0','dtheta','dkappa']
    get_layer_wise_data(layer_files,config)



    for i in tqdm(range(90)):


                    
        hdf,pdf = read_files(i)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)

        doublet_array = make_all_doublets(hits_df,barrel_data,config)
        doublet_fname,triplet_arr_fname,triplet_df_fname,ca_fname = fname(sigma_cut,i)
        with open(doublet_fname, 'wb') as f:
            pickle.dump(doublet_array, f)
        eff,pur,ts,ss = eff_and_pur(doublet_array,signal_df,barrel_data)
        doublet_metrics['eff'][i] = eff
        doublet_metrics['pur'][i] = pur
        doublet_metrics['total_segments'][i] = ts
        doublet_metrics['signal_segments'][i] = ss


        triplet_array,triplet_df = make_triplets(doublet_array,signal_df,hits_df,config,barrel_data) 
        eff,pur,ts,ss = calculate_triplet_metrics(triplet_array,signal_df)
        triplet_metrics['eff'][i] = eff
        triplet_metrics['pur'][i] = pur
        triplet_metrics['total_segments'][i] = ts
        triplet_metrics['signal_segments'][i] = ss

        ca = build_graph(triplet_array,triplet_df)

        
        with open(triplet_arr_fname, 'wb') as f:
            pickle.dump(triplet_array, f)
        with open(triplet_df_fname, 'wb') as f:
            pickle.dump(triplet_df, f)
        with open(ca_fname, 'wb') as f:
            pickle.dump(ca, f)





    doublet_json = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/doublet_metrics.json'
    triplet_json = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/triplet_metrics.json'


    save_dict_as_json(doublet_metrics,doublet_json)
    save_dict_as_json(triplet_metrics,triplet_json)

print("finsihed")
