from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
import yaml
import json
from tqdm import tqdm
import pickle



def calculate_curvature(x1, y1, x2, y2, x3, y3):
    r12_x = x2 - x1
    r12_y = y2 - y1
    r13_x = x3 - x1
    r13_y = y3 - y1
    r23_x = x3 - x2
    r23_y = y3 - y2
    
    r13_mod = np.sqrt(r13_x**2 + r13_y**2)
    r12_mod = np.sqrt(r12_x**2 + r12_y**2)
    r23_mod = np.sqrt(r23_x**2 + r23_y**2)
    
    num = abs(r23_x * r12_y - r12_x * r23_y)
    
    return 2 * num / (r12_mod * r13_mod * r23_mod)



with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
dkappa_dict = {key: [] for key in pdg_dict}
with open("config.yaml", "r") as f:
    # Load and parse the YAML file
    config = yaml.safe_load(f)

barrel_data = get_barrel_data(config)

for key,values in pdg_dict.items():
    for i in tqdm(range(90)):
        hdf,pdf = read_files(i)
        signal_df = get_signal(hdf,pdf,config)
        signal_df = add_barrel_numbers(signal_df,barrel_data)

        pdg_df,pdg_pids = get_hitsdf_for_pdgid(signal_df,pdf,values)
        if(len(pdg_df)):
            pass
        for pid in pdg_pids:
            _df = pdg_df[pdg_df.particle_id==pid]

            barrel_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            frequency_array = np.array([np.sum(_df['barrel_layers'].values == element) for element in barrel_num])
            for i in range(8):
                triplet_layer = frequency_array[i:i+3]
                all_nonzero = np.all(triplet_layer != 0)
                if all_nonzero:
                    xl1 = _df[_df.barrel_layers==barrel_num[i]].x.mean()
                    yl1 = _df[_df.barrel_layers==barrel_num[i]].y.mean()

                    xl2 = _df[_df.barrel_layers==barrel_num[i+1]].x.mean()
                    yl2 = _df[_df.barrel_layers==barrel_num[i+1]].y.mean()

                    xl3 = _df[_df.barrel_layers==barrel_num[i+2]].x.mean()
                    yl3 = _df[_df.barrel_layers==barrel_num[i+2]].y.mean()
                    

                    k013 = calculate_curvature(0,0,xl1,yl1,xl3,yl3)
                    k123 = calculate_curvature(xl1,yl1,xl2,yl2,xl3,yl3)
                    dkappa_dict[key].append(k013-k123)

file_name = 'analysis/dkappa.pkl'

with open(file_name,'wb') as f:
    pickle.dump(dkappa_dict,f)


