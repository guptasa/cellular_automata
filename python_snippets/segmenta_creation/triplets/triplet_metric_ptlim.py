##from doublet arrayes calculates the metrics

from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
import yaml
import json
import numpy as np
from tqdm import tqdm
import pickle


###load doublet and then calculate triplet metrics
pt_arr = [0.5,0.6,0.7,0.8,0.9,1]

track_segments = 'triplets'
lim = 'ptlim'




metrics = {'eff':np.zeros((90,len(pt_arr))),
                   'pur':np.zeros((90,len(pt_arr))),
                   'total_segments':np.zeros((90,len(pt_arr))),
                   'signal_segments':np.zeros((90,len(pt_arr)))}


stage_dir = f'/mnt/data1/gupta/ca/{track_segments}/{lim}/'

def fname(eno,signal,z0,pt):
    doublet_fname = f'/mnt/data1/gupta/ca/doublets/{lim}/'+f'doublets_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname,triplet_fname


def update_metrics(metrics, values, eno, zno):
    metrics['eff'][eno, zno] = values[0]
    metrics['pur'][eno, zno] = values[1]
    metrics['total_segments'][eno, zno] = values[2]
    metrics['signal_segments'][eno, zno] = values[3]

for iter,pt in enumerate(pt_arr):

    print(iter,pt)

    with open("config.yaml", "r") as f:

    # Load and parse the YAML file
        config = yaml.safe_load(f)
    config['pt'] = pt
    config['z0'] = 250
    barrel_data = get_barrel_data(config)    
    
    for i in tqdm(range(90)):

        
        hdf,pdf = read_files(i)
        signal_df = get_signal(hdf,pdf,config)
      
        hits_df = load_barrel_df(hdf)

        doublet_file,triplet_file= fname(i,config['signal'],config['z0'],config['pt'])
        

        with open(doublet_file,'rb') as f:
            doublet_array = np.array(pickle.load(f))

        triplet_dict = make_triplets(doublet_array,signal_df,hits_df,config,barrel_data)
        values = calculate_triplet_metrics(triplet_dict,signal_df)

        # values = eff_and_pur(doublet_array,signal_df,barrel_data)
        update_metrics(metrics,values,i,iter)





json_file = f'analysis/{track_segments}_{lim}_metrics_dpsi_'+str(config['dpsi'])+'.json'
###saving the json file
def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(metrics, default=numpy_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)

print("completed")
##the resulted metrics can be plotted by box plot functions in plotting_utils


##the below snippet is when you load triplets and calculate 

# pt_arr = [0.5,0.6,0.7,0.8,0.9,1]

# track_segments = 'triplets'
# lim = 'ptlim'




# metrics = {'eff':np.zeros((90,len(pt_arr))),
#                    'pur':np.zeros((90,len(pt_arr))),
#                    'total_segments':np.zeros((90,len(pt_arr))),
#                    'signal_segments':np.zeros((90,len(pt_arr)))}


# stage_dir = f'/mnt/data1/gupta/ca/{track_segments}/{lim}/'

# def fname(eno,signal,z0,pt):
#     doublet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
#     triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
#     return doublet_fname,triplet_fname


# def update_metrics(metrics, values, eno, zno):
#     metrics['eff'][eno, zno] = values[0]
#     metrics['pur'][eno, zno] = values[1]
#     metrics['total_segments'][eno, zno] = values[2]
#     metrics['signal_segments'][eno, zno] = values[3]


# def get_fname(eno,signal,z0,pt):
#     fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
#     return fname

# for iter,pt in enumerate(pt_arr):

#     print(iter,pt)

#     with open("config.yaml", "r") as f:

#     # Load and parse the YAML file
#         config = yaml.safe_load(f)
#     config['pt'] = pt
#     config['z0'] = 250
#     barrel_data = get_barrel_data(config)    
    
#     for i in tqdm(range(90)):

        
#         hdf,pdf = read_files(i)
#         signal_df = get_signal(hdf,pdf,config)
#         signel_df = add_barrel_numbers(signal_df,barrel_data)
#         _,triplet_file= fname(i,config['signal'],config['z0'],config['pt'])
        

#         with open(triplet_file,'rb') as f:
#             triplet_dict = pickle.load(f)

 
#         values = calculate_triplet_metrics(triplet_dict,signal_df)

#         # values = eff_and_pur(doublet_array,signal_df,barrel_data)
#         update_metrics(metrics,values,i,iter)





# json_file = f'analysis/{track_segments}_{lim}_metrics_dpsi_'+str(config['dpsi'])+'.json'
# ###saving the json file
# def numpy_encoder(obj):
#     if isinstance(obj, np.int64):
#         return int(obj)
#     elif isinstance(obj, np.ndarray):
#         return obj.tolist()  # Convert NumPy array to Python list
#     raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# # Serialize the dictionary to JSON using the custom encoder function
# json_str = json.dumps(metrics, default=numpy_encoder)

# # Save the JSON string to a file
# with open(json_file, 'w') as f:
#     f.write(json_str)

# print("completed")
# ##the resulted metrics can be plotted by box plot functions in plotting_utils