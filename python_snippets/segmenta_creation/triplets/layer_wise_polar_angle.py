from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)

polar_angle_difference = {i:[] for i in range(8)}

for eno in tqdm(range(90)):            
    hdf,pdf = read_files(eno)

    signal_df = get_signal(hdf,pdf,config)

    barrel_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    signal_triplets = 0
    for pid in signal_df.particle_id.unique():
        df = signal_df[signal_df.particle_id==pid]
        
        frequency_array = np.array([np.sum(df['barrel_layers'].values == element) for element in barrel_num])

        
        for i in range(8):
            triplet_layer = frequency_array[i:i+3]
            all_nonzero = np.all(triplet_layer != 0)
            if all_nonzero:
                ###layer wise mean z and r coordinates

                r1 = df[df.barrel_layers == i].r.mean()
                z1 = df[df.barrel_layers == i].z.mean()
                r2 = df[df.barrel_layers == i+1].r.mean()
                z2 = df[df.barrel_layers == i+1].z.mean()
                r3 = df[df.barrel_layers == i+2].r.mean()
                z3 = df[df.barrel_layers == i+2].z.mean()

                dr1 = r2 - r1
                dz1 = z2 - z1

                theta1 = np.arctan2(dr1,dz1)

                dr2 = r3 - r2
                dz2 = z3 - z2

                theta2 = np.arctan2(dr2,dz2)

                polar_angle_difference[i].append(theta2-theta1)


json_file = 'control_data/polar_angle_difference_layer_wise.json'

def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(polar_angle_difference, default=numpy_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)


print("finsihed")
