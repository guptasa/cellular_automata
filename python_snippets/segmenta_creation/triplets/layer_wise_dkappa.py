from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)



def calculate_curvature(x1, y1, x2, y2, x3, y3):
    r12_x = x2 - x1
    r12_y = y2 - y1
    r13_x = x3 - x1
    r13_y = y3 - y1
    r23_x = x3 - x2
    r23_y = y3 - y2
    
    r13_mod = np.sqrt(r13_x**2 + r13_y**2)
    r12_mod = np.sqrt(r12_x**2 + r12_y**2)
    r23_mod = np.sqrt(r23_x**2 + r23_y**2)
    
    num = abs(r23_x * r12_y - r12_x * r23_y)
    
    return 2 * num / (r12_mod * r13_mod * r23_mod)

dkappa_angle_difference = {i:[] for i in range(8)}

for eno in tqdm(range(90)):            
    hdf,pdf = read_files(eno)

    signal_df = get_signal(hdf,pdf,config)

    barrel_num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    signal_triplets = 0
    for pid in signal_df.particle_id.unique():
        _df = signal_df[signal_df.particle_id==pid]
        
        frequency_array = np.array([np.sum(_df['barrel_layers'].values == element) for element in barrel_num])

        
        for i in range(8):
            triplet_layer = frequency_array[i:i+3]
            all_nonzero = np.all(triplet_layer != 0)
            if all_nonzero:
                ###layer wise mean z and r coordinates
                xl1 = _df[_df.barrel_layers==barrel_num[i]].x.mean()
                yl1 = _df[_df.barrel_layers==barrel_num[i]].y.mean()

                xl2 = _df[_df.barrel_layers==barrel_num[i+1]].x.mean()
                yl2 = _df[_df.barrel_layers==barrel_num[i+1]].y.mean()

                xl3 = _df[_df.barrel_layers==barrel_num[i+2]].x.mean()
                yl3 = _df[_df.barrel_layers==barrel_num[i+2]].y.mean()
                

                k013 = calculate_curvature(0,0,xl1,yl1,xl3,yl3)
                k123 = calculate_curvature(xl1,yl1,xl2,yl2,xl3,yl3)
                dkappa_angle_difference[i].append(k013-k123)


json_file = 'control_data/dkappa_angle_difference_layer_wise.json'

def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(dkappa_angle_difference, default=numpy_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)


print("finsihed")
