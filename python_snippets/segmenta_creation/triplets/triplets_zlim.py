from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
import yaml
import json
from tqdm import tqdm
import pickle

track_segments = 'triplets'
lim = 'zlim'

Z_arr = [150,175,200,225,250,275,300]


metrics = {'eff':np.zeros((90,len(Z_arr))),
                   'pur':np.zeros((90,len(Z_arr))),
                   'total_segments':np.zeros((90,len(Z_arr))),
                   'signal_segments':np.zeros((90,len(Z_arr)))}


def update_metrics(metrics, values, eno, zno):
    metrics['eff'][eno, zno] = values[0]
    metrics['pur'][eno, zno] = values[1]
    metrics['total_segments'][eno, zno] = values[2]
    metrics['signal_segments'][eno, zno] = values[3]


stage_dir = f'/mnt/data1/gupta/ca/{track_segments}/{lim}/'

def fname(eno,signal,z0,pt):
    doublet_fname = f'/mnt/data1/gupta/ca/doublets/{lim}/'+f'doublets_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return doublet_fname,triplet_fname



for iter,z in enumerate(Z_arr):


    with open("config.yaml", "r") as f:

        # Load and parse the YAML file
        config = yaml.safe_load(f)

    config['pt'] = 0.5
    config['z0'] = z
    config['dpsi']=0.03


    barrel_data = get_barrel_data(config)

    for i in tqdm(range(90)):


                    
        hdf,pdf = read_files(i)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)


        doublet_file,triplet_file = fname(i,config['signal'],config['z0'],config['pt'])

        print(doublet_file)
        with open(doublet_file, 'rb') as f:
            doublet_array = pickle.load(f)


        triplet_array,indexes = make_triplets(doublet_array,signal_df,hits_df,config,barrel_data)    


 
        values = calculate_triplet_metrics(triplet_array,signal_df)

        # values = eff_and_pur(doublet_array,signal_df,barrel_data)
        update_metrics(metrics,values,i,iter)





json_file = f'analysis/{track_segments}_{lim}_metrics_dpsi_'+str(config['dpsi'])+'.json'
###saving the json file
def numpy_encoder(obj):
    if isinstance(obj, np.int64):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return obj.tolist()  # Convert NumPy array to Python list
    raise TypeError("Object of type {} is not JSON serializable".format(type(obj)))

# Serialize the dictionary to JSON using the custom encoder function
json_str = json.dumps(metrics, default=numpy_encoder)

# Save the JSON string to a file
with open(json_file, 'w') as f:
    f.write(json_str)


