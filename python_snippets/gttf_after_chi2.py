
# saving triplet metric


from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*
from utils.track_reconstruction_metric_triplet_level  import*
from utils.track_reconstruction_metric_hit_level  import*
from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*
class generated_tracks:
    pass

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:
    pdg_dict = pickle.load(f)
with open("config.yaml", "r") as f:

    # Load and parse the YAML file
    config = yaml.safe_load(f)
barrel_data = get_barrel_data(config)
layer_files = ['z0','dtheta','dkappa']
get_layer_wise_data(layer_files,config)
def fname(sigma_cut,eno):


    stage_dir = f'/mnt/data1/gupta/ca/{sigma_cut}' +'sigma/final_config/'

    triplet_arr_fname = stage_dir+f'triplets/triplet_arr{eno}.pkl'
    triplet_df_fname = stage_dir+f'triplets/triplet_df{eno}.pkl'
    ca_fname = f'generated_tracks/ca_class_eno_{eno}.pkl'
    # triplet_fname = stage_dir+f'{track_segments}_eventno_'+str(eno)+'_signal_'+str(signal)+'_z0_'+str(z0)+'_pt_'+str(pt)+'.pkl'
    return triplet_arr_fname,triplet_df_fname,ca_fname

triplet_metric = { 'eff' : [] , 'pur' : [] , 'total_triplets' : [] , 'signal_triplets' : [] }
triplet_metric_after_chi2 = { 'eff' : [] , 'pur' : [] , 'total_triplets' : [] , 'signal_triplets' : [] }

config['chi_square_cut'] = 7

for eno in tqdm(range(90)):
    
    hdf,pdf = read_files(eno)
    hits_df = load_barrel_df(hdf)
    signal_df = get_signal(hdf,pdf,config)

    triplet_arr_fname,triplet_df_fname,ca_fname = fname(config['sigma_cut_doublets'],eno)


    with open(triplet_arr_fname,'rb') as f:
        triplet_arr = pickle.load(f)
    with open(triplet_df_fname,'rb') as f:
        triplet_df = pickle.load(f)
     

    # filtered_triplet_arr,filtered_triplet_df = triplet_array_after_chi2_cut(triplet_arr,triplet_df,config)    

    eff,pur,total_triplets,signal_triplets = calculate_triplet_metrics(triplet_arr,signal_df)
    
    triplet_metric['eff'].append(eff)
    triplet_metric['pur'].append(pur)
    triplet_metric['total_triplets'].append(total_triplets)
    triplet_metric['signal_triplets'].append(signal_triplets)

    ##apply chi2 cut 
    ca,triplet_arr_chi2,triplet_df_chi2 = build_graph_after_chi2_cut(triplet_arr,triplet_df,config)

    eff,pur,total_triplets,signal_triplets = calculate_triplet_metrics(triplet_arr_chi2,signal_df)

    triplet_metric_after_chi2['eff'].append(eff)
    triplet_metric_after_chi2['pur'].append(pur)
    triplet_metric_after_chi2['total_triplets'].append(total_triplets)
    triplet_metric_after_chi2['signal_triplets'].append(signal_triplets)



    eff,pur,per_eff,track_collection_hits = track_reco_meric(ca.track_collection,
                 triplet_arr_chi2,
                 triplet_df_chi2,
                 hits_df,
                 signal_df)

    signal_pids = signal_df['particle_id'].unique()

    ca.ca_metric = {'eff':eff,'pur':pur,'per_eff':per_eff,'total_tracks':len(ca.track_collection)}

    print(eff,pur,per_eff,len(ca.track_collection))



    chi2_gttf_array= [] ## array for storing track quality of each track
    c3d_gttf_array = [] ## array for storing track quality of each track
    label = []

    for track in ca.track_collection:
        triplet_object_array = []
        hits = []
        for iter,element in enumerate(track):
            iter  = 7-iter
            index_layer = np.where(triplet_df_chi2 == iter)[0]

            triplet_element = triplet_arr_chi2[index_layer][element]

            triplet_object_array.append(triplet_element)
            hits.extend(triplet_element.hitids)

        hits = np.unique(hits) 
        triplet_object_array.reverse()

        pid_ = hits_df[hits_df.hit_id.isin(hits)]['particle_id'].values  ##finsding particle id for all hits in a track
        unique, counts = np.unique(pid_, return_counts=True)
        
        if(np.max(counts)==10):  ##criterion for 100% matching 
            if pid_[0] in signal_pids:
                label.append(True)  
            else:
                label.append(False)         
        else:
            label.append(False)    

        gttf_class = GTTF(triplet_object_array)
        
        chi2_gttf_array.append(gttf_class.chi2_glob)
        c3d_gttf_array.append(gttf_class.C3D_glob)

    ca.chi2_gttf_array = np.array(chi2_gttf_array)
    ca.c3d_gttf_array = np.array(c3d_gttf_array)

    ca.pt = 0.3*2/np.abs(c3d_gttf_array)

    ca.label = np.array(label)

    with open(ca_fname,'wb') as f:
        pickle.dump(ca,f)

triplet_metric_fname = 'triplet_metric.json'
triplet_metric_after_chi2_fname = 'triplet_metric_after_chi2.json'

save_dict_as_json(triplet_metric,triplet_metric_fname)
save_dict_as_json(triplet_metric_after_chi2,triplet_metric_after_chi2_fname)