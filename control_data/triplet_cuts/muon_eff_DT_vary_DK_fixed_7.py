from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*

from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:

    pdg_dict = pickle.load(f)

DT  = [1,2,3,4,5,6]


event_name = 'event_array.pkl'
with open(event_name,'rb') as f:
    event_array = pickle.load(f)

def eandp(reco_doublet_arr,signal_df,barrel_data):
    signal_truth = 0
    total_doublets = 0
    
    pid = signal_df.particle_id.unique()
    for doublets in reco_doublet_arr:
        true_doublets = sum(doublets.labels)
      
        if(true_doublets):

            if(np.isin(doublets.pid1,pid)):
               signal_truth += true_doublets

        total_doublets += len(doublets.labels)

    pur = signal_truth/total_doublets 
    signal_doublets = total_signal_doublets(signal_df,barrel_data)

    eff = signal_truth/signal_doublets

    return signal_truth,signal_doublets

eff_trip = []




for dtheta in DT:
    muon_reco = 0
    muon_truth = 0
        
    with open("config.yaml", "r") as f:

        # Load and parse the YAML file
        config = yaml.safe_load(f)
    config['sigma_cut_triplets'][0] = dtheta    
    config['sigma_cut_triplets'][1] = 7
    
    barrel_data = get_barrel_data(config)

    layer_files = ['z0','dtheta','dkappa']
 
    get_layer_wise_data(layer_files,config)


    for eno in tqdm(event_array):
        hdf,pdf = read_files(eno)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)


        total_signal_pids = signal_df['particle_id'].unique()

        ##getting particle type from pdf dataframe
        particle_signal_df = pdf[pdf.particle_id.isin(total_signal_pids)]
        pdg_pdf = particle_signal_df[particle_signal_df['particle_type'].isin([-13,13])]

        pdg_pids = pdg_pdf['particle_id'].unique()



        pdg_df = signal_df[signal_df['particle_id'].isin(pdg_pids)]
        if(len(pdg_df)):
            # doublet_array = make_all_doublets(pdg_df,barrel_data,config)

            # signal_truth,signal_doublet = eandp(doublet_array,pdg_df,barrel_data)
            doublet_array = make_all_doublets(pdg_df,barrel_data,config)

            triplet_array,triplet_df = make_triplets(doublet_array,pdg_df,hits_df,config,barrel_data) 
            eff,pur,ts,constructed_true_triplets = calculate_triplet_metrics(triplet_array,pdg_df)
            signal_triplets = calculate_signal_triplets(pdg_df)
            

            muon_reco = muon_reco + constructed_true_triplets
            muon_truth = muon_truth + signal_triplets
    e = muon_reco/muon_truth
    eff_trip.append(e)    
        
eff_trip_name = 'control_data/triplet_cuts/muon_triplet_eff_dt_vary_dk_7.pkl'

with open(eff_trip_name,'wb') as f:
    pickle.dump(eff_trip,f)

dt_array_name = 'control_data/triplet_cuts/dt_array.pkl' 
with open(dt_array_name,'wb') as f:
    pickle.dump(DT,f)   