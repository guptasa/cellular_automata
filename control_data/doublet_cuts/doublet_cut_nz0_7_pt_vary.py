from utils.readfile import*
from utils.barrel_data import*
from utils.doublet import*
from utils.triplet import*
from utils.plotting_utils import*

from importlib import reload
import yaml
import json
from tqdm import tqdm
import pickle
from utils.cellular_automata import*
from utils.triplet_fit import*

pd.set_option('display.max_columns', None) 

with open('files/pdgids.pkl','rb') as f:

    pdg_dict = pickle.load(f)

PT  = [0.4,0.5,0.6,0.7,0.8,0.9,1.0]
metric = {i:[] for i in PT}
total_muons = 0
event_name = 'event_array.pkl'
with open(event_name,'rb') as f:
    event_array = pickle.load(f)

def eandp(reco_doublet_arr,signal_df,barrel_data):
    signal_truth = 0
    total_doublets = 0
    
    pid = signal_df.particle_id.unique()
    for doublets in reco_doublet_arr:
        true_doublets = sum(doublets.labels)
      
        if(true_doublets):

            if(np.isin(doublets.pid1,pid)):
               signal_truth += true_doublets

        total_doublets += len(doublets.labels)

    pur = signal_truth/total_doublets 
    signal_doublets = total_signal_doublets(signal_df,barrel_data)

    eff = signal_truth/signal_doublets

    return signal_truth,signal_doublets

eff2 = []


for pt in PT:
    muon_reco = 0
    muon_truth = 0
        
    with open("config.yaml", "r") as f:

        # Load and parse the YAML file
        config = yaml.safe_load(f)
    config['pt'] = pt    
    barrel_data = get_barrel_data(config)

    layer_files = ['z0','dtheta','dkappa']
 
    get_layer_wise_data(layer_files,config)


    for eno in tqdm(event_array):
        hdf,pdf = read_files(eno)

        signal_df = get_signal(hdf,pdf,config)

        if(config['signal']):
            hits_df = signal_df
        else:
            hits_df = load_barrel_df(hdf)


        total_signal_pids = signal_df['particle_id'].unique()

        ##getting particle type from pdf dataframe
        particle_signal_df = pdf[pdf.particle_id.isin(total_signal_pids)]
        pdg_pdf = particle_signal_df[particle_signal_df['particle_type'].isin([-13,13])]

        pdg_pids = pdg_pdf['particle_id'].unique()



        pdg_df = signal_df[signal_df['particle_id'].isin(pdg_pids)]
        if(len(pdg_df)):
            doublet_array = make_all_doublets(pdg_df,barrel_data,config)

            signal_truth,signal_doublet = eandp(doublet_array,pdg_df,barrel_data)

            muon_reco = muon_reco + signal_truth
            muon_truth = muon_truth + signal_doublet
    e = muon_reco/muon_truth
    eff2.append(e)    
        
